<?php

namespace app\Helpers;

use app\Models\mBarang;
use app\Models\mHutangLain;
use app\Models\mHutangSupplier;
use app\Models\mKaryawan;
use app\Models\mPelanggan;
use app\Models\mPembelian;
use app\Models\mPenjualan;
use app\Models\mPiutangLain;
use app\Models\mPiutangPelanggan;
use app\Models\mUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Config;

use Illuminate\View\View;


class Main
{
    public static $date_format_view = 'd F Y H:i';
    public static $error = 'error';
    public static $success = 'success';
    public static $get = 'Success getting data';
    public static $store = 'Storing data success';
    public static $update = 'Updating data success';
    public static $delete = 'Removing data success';
    public static $import = 'Importing data success';
    public static $website_official = 'http://intiru.com';
    public static $rajaongkir_key = '2ecc568bbed69df540d3a5941b74b2ca';
    public static $komisi_penjual_inbis_dan_marketer = 0.2;
    public static $wa_number_admin = '6287866108551';


    public static function data($breadcrumb = array(), $menuActive = '')
    {
        $user = Session::get('user');
        $user_role = Session::get('user_role');
//        $users = mUser::where('id', $user->id)->with('user_role')->first();

        $user_foto = $user->karyawan->foto_karyawan;
        $user_nama = $user->karyawan->nama_karyawan;
        $user_email = $user->karyawan->email_karyawan;
        $user_saldo = mKaryawan::where('id', $user->id_karyawan)->value('saldo_karyawan');

        $data['menu'] = Main::generateTopMenu($menuActive);
        $data['menuAction'] = Main::menuActionData($menuActive);
        $data['footer'] = Main::footer();
        $data['metaTitle'] = Main::metaTitle($breadcrumb);
        $data['pageTitle'] = Main::pageTitle($breadcrumb);
        $data['breadcrumb'] = Main::breadcrumb($breadcrumb);
        $data['user'] = $user;
        $data['user_role_data'] = $user_role['role_akses'];
        $data['user_role_name'] = $user_role['role_name'];
        $data['user_foto'] = $user_foto;
        $data['user_nama'] = $user_nama;
        $data['user_email'] = $user_email;
        $data['pageMethod'] = '';
        $data['no'] = 1;
        $data['imgWidth'] = 100;
        $data['decimalStep'] = '.01';
        $data['roundDecimal'] = 2;
        $data['ppnPersen'] = Config::get('constants.ppnPersen');
        $data['methodProses'] = '';
        $data['user_saldo'] = $user_saldo;


        $data['cons'] = Config::get('constans');

        return $data;
    }

    public static function whatsapp_link_withdraw()
    {
        $user = Session::get('user');
        $user_nama = $user->karyawan->nama_karyawan;
        $user_phone = $user->karyawan->telp_karyawan;
        $user_alamat = $user->karyawan->alamat_karyawan;
        $user_saldo = mKaryawan::where('id', $user->id_karyawan)->value('saldo_karyawan');

        $message = '
Hai Admin,
            
Saya mau melakukan withdraw dengan identitas :
Nama : '.$user_nama.'
Nomer Telepon : '.$user_phone.'
Alamat : '.$user_alamat.'
Total Withdraw : '.Main::format_money($user_saldo).'
            
Terima Kasih
        ';

        return urlencode($message);
    }

    public static function companyInfo()
    {
        $data = [
            'bankType' => Config::get('constants.bankType'),
            'bankRekening' => Config::get('constants.bankRekening'),
            'bankAtasNama' => Config::get('constants.bankAtasNama'),
            'companyName' => Config::get('constants.companyName'),
            'companyAddress' => Config::get('constants.companyAddress'),
            'companyPhone' => Config::get('constants.companyPhone'),
            'companyTelp' => Config::get('constants.companyTelp'),
            'companyEmail' => Config::get('constants.companyEmail'),
            'companyBendahara' => Config::get('constants.companyBendahara'),
            'companyTuju' => Config::get('constants.companyTuju'),
        ];

        $data = (object)$data;

        return $data;
    }

    public static function response($status = 'error', $message = 'Empty', $data = NULL, $errors = NULL)
    {
        return [
            'status' => $status,
            'message' => $message,
            'data' => $data,
            'errors' => $errors
        ];
    }

    public static function access_menu_first()
    {
        $user_role = Session::get('user_role')->role_akses;
        $user_role = json_decode($user_role, TRUE);
        $first_menu_active_route = '';
        $cons = Config::get('constants.topMenu');
        $main_menu = Main::menuAdministrator();

        foreach ($user_role as $key => $row) {
            if ($row['akses_menu']) {
                $first_menu_active_route = $main_menu[$cons[$key]]['route'];
                break;
            }
        }

        return $first_menu_active_route;
    }

    public static function totalNotification($notifications)
    {
        return count($notifications);
    }

    public static function badgeNotification($totalNotification)
    {
        if ($totalNotification > 0) {
            return '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot m-badge--danger"></span>';
        }
        return '';
    }

    public static function totalAlertBahan()
    {
        $list = mBahan
            ::with(
                'stok_bahan:id_bahan,qty'
            )
            ->get();
        $totalAlert = 0;

        foreach ($list as $r) {
            $countQty = 0;
            foreach ($r->stok_bahan as $r2) {
                $countQty += $r2->qty;
            }

            if ($r->minimal_stok > $countQty) {
                $totalAlert++;
            }
        }

        return $totalAlert;
    }

    public static function totalAlertBarang()
    {
        $data_list = mBarang
            ::with('jenis_barang')
            ->withCount([
                'stok_barang AS total_stok' => function ($query) {
                    $query->select(DB::raw('SUM(sbr_qty)'));
                }
            ])
            ->get();

        $total_data = 0;
        foreach ($data_list as $row) {
            if ($row->brg_minimal_stok >= $row->total_stok) {
                $total_data++;
            }
        }


        return $total_data;
    }

    public static function badgeAlertBarang($totalAlert = '')
    {
        if ($totalAlert == '') {
            $totalAlert = Main::totalAlertBarang();
        }

        if ($totalAlert > 0) {
            return '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">' . $totalAlert . '</span></span>';
        }
        return '';
    }

    public static function badgeAlertBahan($totalAlert = '')
    {
        if ($totalAlert == '') {
            $totalAlert = Main::totalAlertBahan();
        }

        if ($totalAlert > 0) {
            return '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">' . $totalAlert . '</span></span>';
        }
        return '';
    }

    public static function bagdeInventory($totalAlert = '')
    {
        if ($totalAlert > 0) {
            return '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">' . $totalAlert . '</span></span>';
        }
        return '';
    }

    public static function notifAlertBahan()
    {
//        $totalAlert = Main::totalAlertBahan();
//        if ($totalAlert > 0) {
//            return '
//                <a href="' . route('stokAlertBahan') . '" class="m-list-timeline__item">
//                    <span class="m-list-timeline__badge"></span>
//                    <span href="" class="m-list-timeline__text">Stok Bahan kurang dari Minimal Stok, segera setarakan. <span class="m-badge m-badge--danger m-badge--wide">' . $totalAlert . '</span></span>
//                </a>
//            ';
//        }
        return '';
    }

    public static function notifAlertProduk()
    {
//        $totalAlert = Main::totalAlertProduk();
//        if ($totalAlert > 0) {
//            return '
//                <a href="' . route('stokAlertProduk') . '" class="m-list-timeline__item">
//                    <span class="m-list-timeline__badge"></span>
//                    <span href="" class="m-list-timeline__text">Stok Produk kurang dari Minimal Stok, segera setarakan.
//                    <span class="m-badge m-badge--danger m-badge--wide">' . $totalAlert . '</span></span>
//                </a>
//            ';
//        }
        return '';
    }

    public static function generateTopMenu($menuActive)
    {
//        $totalAlertBahan = Main::totalAlertBahan();
//        $totalAlertProduk = Main::totalAlertProduk();

        $data['routeName'] = Route::currentRouteName();
        $data['menu'] = Main::menuList();
        $data['menuActive'] = $menuActive;
        $data['consMenu'] = Config::get('constants.topMenu');
        $data['stokAlertBadge'] = Main::badgeAlertBarang();
//        $data['badgeAlertBahan'] = Main::badgeAlertBahan($totalAlertBahan);
//        $data['badgeAlertProduk'] = Main::badgeAlertProduk($totalAlertProduk);
//        $data['badgeInventory'] = Main::bagdeInventory($totalAlertBahan + $totalAlertProduk);
//

        return view('component/menu', $data);
    }

    public static function footer()
    {
        $data = [];
        return view('component/footer', $data);
    }

    public static function format_money($number)
    {
        if (Main::check_decimal($number)) {
            return 'Rp. ' . number_format($number, 2, ',', '.');
        } else {
            return 'Rp. ' . number_format($number, 2, ',', '.');
        }
    }

    public static function unformat_money($number)
    {
        $number = str_replace(['Rp', ''], ['.', ''], [',', '.'], $number);
        $number = self::format_decimal($number);
        return self::format_number($number);
    }

    public static function format_number($number)
    {
        if (Main::check_decimal($number)) {
            return number_format($number, 2, '.', ',');
        } else {
            return number_format($number, 0, '', ',');
        }
    }

    public static function format_number_system($number)
    {
        return number_format($number, 2, ',', '.');
    }

    public static function format_number_db($number)
    {
        $number = str_replace(['.', ','], ['.', ''], $number);
        return number_format($number, 2, '.', '');
    }

    public static function format_decimal($number)
    {
        $number = str_replace(['.', ','], ['.', ''], $number);
        return number_format($number, 0, ',', '.');
    }

    public static function format_discount($number)
    {
        return $number . ' %';
    }

    public static function format_date($date)
    {
        return date('d-m-Y', strtotime($date));
    }

    public static function format_datetime_input($date)
    {
        return date('d-m-Y H:i', strtotime($date));
    }

    public static function date()
    {
        return date('Y-m-d');
    }

    public static function datetime()
    {
        return date('Y-m-d H:i:s');
    }

    public static function format_date_label($date)
    {
        return date('d F Y', strtotime($date));
    }

    public static function format_date_db($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public static function format_datetime_db($date)
    {
        return date('Y-m-d H:i:s', strtotime($date));
    }

    public static function format_datetime($date)
    {
        return date('d F Y H:i:s', strtotime($date));
    }

    public static function format_datetime_2($date)
    {
        return date('d-m-Y H:i:s', strtotime($date));
    }

    public static function format_time_db($time)
    {
        return date('H:i:s', strtotime($time));
    }

    public static function format_time_label($time)
    {
        return date('h:i A', strtotime($time));
    }

    public static function format_age($date)
    {
        $year = date_diff(date_create($date), date_create('today'))->y;
        $month = date_diff(date_create($date), date_create('today'))->m;
        $label = $year . ' Tahun ' . $month . ' Bulan';

        return $label;
    }

    public static function age($date)
    {
        $year = date_diff(date_create($date), date_create('today'))->y;
        $month = date_diff(date_create($date), date_create('today'))->m;
        $label = $year;

        return $label;
    }

    public static function need_type_label($label, $control_step = '')
    {
        switch ($label) {
            case "consult":
                return 'Konsultasi';
                break;
            case "action":
                return 'Tindakan';
                break;
            case "control":
                return 'Kontrol ke ' . $control_step;
                break;
            case "done":
                return 'Selesai';
                break;
            case "cancel":
                return 'Batal';
                break;
            default:
                return $label;
        }
    }

    public static function need_type_class($label)
    {
        switch ($label) {
            case "consult":
                return 'm-fc-event--light m-fc-event--solid-warning';
                break;
            case "action":
                return 'm-fc-event--solid-info m-fc-event--light';
                break;
            case "control":
                return 'm-fc-event--light m-fc-event--solid-success';
                break;
            case "done":
                return 'm-fc-event--light m-fc-event--solid-primary';
                break;
            case "cancel":
                return '';
                break;
            default:
                return $label;
        }
    }

    public static function need_type_style($label, $control_step = '')
    {
        switch ($label) {
            case "consult":
                return '<span style="background-color: #FFB822; padding: 4px 8px; border-radius: 4px">Konsultasi</span>';
                break;
            case "action":
                return '<span style="background-color: #36A3F6; padding: 4px 8px; border-radius: 4px; color: white">Tindakan</span>';
                break;
            case "control":
                return '<span style="background-color: #34BFA3; padding: 4px 8px; border-radius: 4px; color: white">Kontrol ke ' . $control_step . '</span>';
                break;
            case "done":
                return '<span style="background-color: #5c2fba; padding: 4px 8px; border-radius: 4px; color: white">Selesai</span>';
                break;
            case "cancel":
                return '<span style="background-color: #5c2fba; padding: 4px 8px; border-radius: 4px">Batal</span>';
                break;
            default:
                return $label;
        }
    }

    public static function need_type_class_reguler($label)
    {
        switch ($label) {
            case "consult":
                return 'm-badge m-badge--warning m-badge--wide';
                break;
            case "action":
                return 'm-badge m-badge--info m-badge--wide';
                break;
            case "control":
                return 'm-badge m-badge--success m-badge--wide';
                break;
            case "done":
                return 'm-badge m-badge--primary m-badge--wide';
                break;
            case "cancel":
                return '';
                break;
            default:
                return $label;
        }
    }

    public static function need_type_span($label_raw, $addt = '')
    {

        if ($addt && $label_raw == 'control') {
            $addt = ' ke ' . $addt;
        } else {
            $addt = '';
        }

        $label = Main::need_type_label($label_raw);
        $class = Main::need_type_class_reguler($label_raw);
        $span = '<span class="' . $class . '">' . $label . $addt . '</span>';

        return $span;
    }

    public static function convert_money($str)
    {
        $find = array('Rp', '.', '_', ' ');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function encrypt($id)
    {
        return Crypt::encrypt($id);
    }

    public static function decrypt($id)
    {
        return Crypt::decrypt($id);
    }

    public static function convert_number($str)
    {
        $find = array('.', '_', ' ');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function convert_discount($str)
    {
        $find = array('%', ' ', '_');
        $replace = array('');
        return str_replace($find, $replace, $str);
    }

    public static function check_decimal($number)
    {
        if ($number - floor($number) >= 0.1) {
            return true;
        } else {
            return false;
        }
    }

    public static function metaTitle($breadcrumb)
    {
        krsort($breadcrumb);
        $title = '';
        foreach ($breadcrumb as $label => $value) {
            $title .= Main::menuAction($value['label']) . ' < ';
        }

        $title .= env("APP_NAME", "Program Penjualan");

        return $title;

    }

    public static function pageTitle($breadcrumb = array())
    {
        krsort($breadcrumb);
        $title = isset($breadcrumb[1]) ? Main::menuAction($breadcrumb[1]['label']) : Main::menuAction($breadcrumb[0]['label']);

        return $title;

    }

    public static function menuAction($string)
    {
        $string = str_replace('_', ' ', $string);
        $string = ucwords($string);

        return $string;
    }

    public static function menuStrip($string)
    {
        return str_replace([' ', '/'], '_', strtolower($string));
    }

    public static function menuActionData($menuActive)
    {
        $menuList = Main::menuAdministrator();
        $routeName = Route::currentRouteName();
        $userRole = json_decode(Session::get('user.user_role.role_akses'), TRUE);
        //$menuActive = '';
        $action = [];

        if ($menuActive == '') {
            foreach ($menuList as $menu => $val) {
                if ($val['route'] == $routeName) {
                    $menuActive = $menu;
                    break;
                } else {
                    if (isset($val['sub'])) {
                        foreach ($val['sub'] as $menu_sub => $val_sub) {
                            if ($val_sub['route'] == $routeName) {
                                $menuActive = $menu_sub;
                            }
                        }
                    }
                }
            }
        }
        if ($userRole) {
            foreach ($userRole as $menuName => $menuVal) {
                if ($menuName == $menuActive) {
                    $action = $menuVal;
                } else {
                    foreach ($menuVal as $menuSubName => $menuSubVal) {
                        if ($menuSubName == $menuActive) {
                            $action = $menuSubVal;
                        }
                    }
                }
            }
        }

        return $action;

    }

    public static function string_to_number($text)
    {
        return str_replace(',', '', $text);
    }

    public static function breadcrumb($breadcrumb_extend = array())
    {
        $cons = Config::get('constants.topMenu');

        $breadcrumb[] = [
            'label' => $cons['dashboard'],
            'route' => route('dashboardPage')
        ];

        $data['breadcrumb'] = array_merge($breadcrumb, $breadcrumb_extend);
        return view('component.breadcrumb', $data);

    }

    public static function no_seri_produk($month, $year, $id_produk, $id_lokasi, $urutan)
    {
        $id_kategori_produk = mProduk::select('id_kategori_produk')->where('id', $id_produk)->first()->id_kategori_produk;
        $kode_kategori_produk = mKategoriProduk::select('kode_kategori_produk')->where('id', $id_kategori_produk)->first()->kode_kategori_produk;
        $kode_lokasi = mLokasi::select('kode_lokasi')->where('id', $id_lokasi)->first()->kode_lokasi;

        return $month . $year . $kode_kategori_produk . $kode_lokasi . $urutan;
    }

    public static function urutan_produk($id_produk, $id_lokasi, $month, $year)
    {
        $urutan = 1;
        $where = [
            'id_produk' => $id_produk,
            'id_lokasi' => $id_lokasi,
            'month' => $month,
            'year' => $year
        ];

        $stok_urutan = mStokProduk::where($where);

        if ($stok_urutan->count() > 0) {
            $urutan_now = $stok_urutan->select('urutan')->orderBy('urutan', 'DESC')->first()->urutan;
            $urutan = $urutan_now + 1;
        }

        return $urutan;
    }

    public static function patient_status($status)
    {
        switch ($status) {
            case "appointment":
                return '<span class="m-badge m-badge--brand m-badge--wide">Appointment</span>';
                break;
            case "consult":
                return '<span class="m-badge m-badge--warning m-badge--wide">Konsultasi</span>';
                break;
            case "action":
                return '<span class="m-badge m-badge--info m-badge--wide">Finalize</span>';
                break;
            case "control":
                return '<span class="m-badge m-badge--success m-badge--wide">Follow Up</span>';
                break;
            case "done":
                return '<span class="m-badge m-badge--brand m-badge--wide">Selesai</span>';
                break;
            case "cancel":
                return '<span class="m-badge m-badge--danger m-badge--wide">Batal</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    public static function patient_status_raw($status)
    {
        switch ($status) {
            case "appointment":
                return 'Appointment';
                break;
            case "consult":
                return 'Konsultasi';
                break;
            case "action":
                return 'Finalize';
                break;
            case "control":
                return 'Follow Up';
                break;
            case "done":
                return 'Selesai';
                break;
            case "cancel":
                return 'Batal';
                break;
            default :
                return '-';
        }
    }

    public static function status($status)
    {
        switch ($status) {
            case "yes":
                return '<span class="m-badge m-badge--info m-badge--wide">Ya</span>';
                break;
            case "no":
                return '<span class="m-badge m-badge--warning m-badge--wide">Tidak</span>';
                break;
            default :
                return '<span class="m-badge m-badge--metal m-badge--wide">-</span>';
        }
    }

    /**
     * @return array
     *
     * Menu variable
     */
    public static function menuList()
    {

        return Main::menuAdministrator();

    }

    public static function menuAdministrator()
    {
        $cons = Config::get('constants.topMenu');

        return [
            $cons['dashboard'] => [
                'icon' => 'flaticon-dashboard',
                'route' => 'dashboardPage',
                'action' => ['list']
            ],
            $cons['masterData'] => [
                'icon' => 'flaticon-cogwheel-2',
                'route' => '#',
                'sub' => [
                    $cons['master_satuan'] => [
                        'route' => 'satuanList',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_jenis_barang'] => [
                        'route' => 'jenisBarangList',
                        'action' => [
                            'list',
                            'menu_akses',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_supplier'] => [
                        'route' => 'supplierList',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_pelanggan'] => [
                        'route' => 'pelangganList',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_user'] => [
                        'route' => 'userPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_karyawan'] => [
                        'route' => 'karyawanPage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['master_role_user'] => [
                        'route' => 'userRolePage',
                        'action' => [
                            'list',
                            'create',
                            'edit',
                            'delete'
                        ]
                    ],
                ],
            ],

            $cons['pembelian'] => [
                'icon' => 'flaticon-file-2',
                'route' => 'pembelianList',
                'action' => [
                    'list',
                    'create_admin',
                    'create_member',
                    'detail',
                    'edit',
                    'delete'
                ]
            ],

            $cons['master_barang'] => [
                'icon' => 'flaticon-file-2',
                'route' => 'barangList',
                'action' => [
                    'list',
                    'create',
                    'edit',
                    'delete'
                ]
            ],

            $cons['penjualan'] => [
                'icon' => 'flaticon-file-2',
                'route' => 'penjualanList',
                'action' => [
                    'list',
                    'create_admin',
                    'create_member',
                    'detail',
                    'edit',
                    'delete'
                ]
            ],

            $cons['penyesuaian_stok'] => [
                'icon' => 'flaticon-file-2',
                'route' => 'penyesuaianStokList',
                'action' => [
                    'list',
                    'create_admin',
                    'create_member',
                    'detail',
                    'edit',
                    'delete'
                ]
            ],

            $cons['kartu_stok'] => [
                'icon' => 'flaticon-file-2',
                'route' => 'kartuStokList',
                'action' => [
                    'list',
                    'create_admin',
                    'create_member',
                    'detail',
                    'edit',
                    'delete'
                ]
            ],

            $cons['stok_alert'] => [
                'icon' => 'flaticon-file-2',
                'route' => 'stokAlertList',
                'action' => [
                    'list',
                    'create_admin',
                    'create_member',
                    'detail',
                    'edit',
                    'delete'
                ]
            ],

            /*            $cons['retur'] => [
                            'icon' => 'flaticon-file-2',
                            'route' => 'appointmentList',
                            'action' => [
                                'list',
                                'create_admin',
                                'create_member',
                                'detail',
                                'edit',
                                'delete'
                            ]
                        ],*/
            $cons['hutang_piutang'] => [
                'icon' => 'flaticon-cogwheel-2',
                'route' => '#',
                'sub' => [
                    $cons['hutang_lain_lain'] => [
                        'icon' => 'flaticon-file-2',
                        'route' => 'hutangLainList',
                        'action' => [
                            'list',
                            'create_admin',
                            'create_member',
                            'detail',
                            'edit',
                            'delete'
                        ]
                    ],
                    $cons['hutang_supplier'] => [
                        'icon' => 'flaticon-file-2',
                        'route' => 'hutangSupplierList',
                        'action' => [
                            'list',
                            'create_admin',
                            'create_member',
                            'detail',
                            'edit',
                            'delete'
                        ]
                    ],

                    $cons['piutang_lain_lain'] => [
                        'icon' => 'flaticon-file-2',
                        'route' => 'piutangLainList',
                        'action' => [
                            'list',
                            'create_admin',
                            'create_member',
                            'detail',
                            'edit',
                            'delete'
                        ]
                    ],

                    $cons['piutang_pelanggan'] => [
                        'icon' => 'flaticon-file-2',
                        'route' => 'piutangPelangganList',
                        'action' => [
                            'list',
                            'create_admin',
                            'create_member',
                            'detail',
                            'edit',
                            'delete'
                        ]
                    ],
                ],

            ],

            $cons['laporan'] => [
                'icon' => 'flaticon-file-2',
                'route' => 'laporanList',
                'action' => [
                    'list',
                    'create_admin',
                    'create_member',
                    'detail',
                    'edit',
                    'delete'
                ]
            ],


            /*            $cons['appointment'] => [
                            'icon' => 'flaticon-cogwheel-2',
                            'route' => 'appointmentList',
                            'action' => [
                                'list',
                                'create_admin',
                                'create_member',
                                'detail',
                                'edit',
                                'delete'
                            ]
                        ],
                        $cons['consult'] => [
                            'icon' => 'flaticon-file-2',
                            'route' => 'consultList',
                            'action' => [
                                'list',
                                'consult_done',
                                'consult_cancel',
                                'edit',
                                'detail'
                            ]
                        ],
                        $cons['action'] => [
                            'icon' => 'flaticon-file-2',
                            'route' => 'actionProcessList',
                            'action' => [
                                'action_wait_list',
                                'action_wait_done',
                                'action_wait_back',
                                'action_wait_cancel',
                                'action_wait_edit',
                                'action_wait_detail',

                                'action_done_list',
                                'action_done_edit',
                                'action_done_detail'
                            ]
                        ],
                        $cons['control'] => [
                            'icon' => 'flaticon-file-2',
                            'route' => 'controlList',
                            'action' => [
                                'list',
                                'control_done',
                                'control_back',
                                'edit',
                                'detail'
                            ]
                        ],
                        $cons['done'] => [
                            'icon' => 'flaticon-file-2',
                            'route' => 'doneList',
                            'action' => [
                                'list',
                                'medic_record_print',
                                'payment_print',
                                'done_back',
                                'detail'
                            ]
                        ],
                        $cons['payment'] => [
                            'icon' => 'flaticon-list-1',
                            'route' => 'paymentList',
                            'action' => [
                                'list',
                                'print',
            //                    'detail',
                                'edit',
            //                    'delete'
                            ]
                        ],
                        $cons['schedule_calendar'] => [
                            'icon' => 'flaticon-list-3',
                            'route' => 'scheduleCalendarPage',
                            'action' => [
                                'list',
                            ]
                        ],
                        $cons['patient'] => [
                            'icon' => 'flaticon-users-1',
                            'route' => 'patientList',
                            'action' => [
                                'list',
                                'medic_record_print',
                                'medic_record_edit',
                                'detail',
                                'edit',
                                'delete',
                            ]
                        ],
                        $cons['reminder'] => [
                            'icon' => 'flaticon-cogwheel-2',
                            'route' => '#',
                            'sub' => [
                                $cons['reminder_setting'] => [
                                    'icon' => 'flaticon-list-1',
                                    'route' => 'reminderSettingList',
                                    'action' => [
                                        'list',
                                        'update'
                                    ]
                                ],
                                $cons['reminder_history'] => [
                                    'icon' => 'flaticon-time',
                                    'route' => 'reminderHistoryList',
                                    'action' => [
                                        'list',
                                        'detail'
                                    ]
                                ],
                                $cons['whatsapp_link'] => [
                                    'icon' => 'flaticon-time',
                                    'route' => 'whatsAppLink',
                                    'action' => [
                                        'list'
                                    ]
                                ],
                            ]
                        ],*/

        ];
    }

    public static function menuDistributor()
    {
        $cons = Config::get('constants.topMenu');
        return [
            $cons['dashboard'] => [
                'icon' => 'flaticon-dashboard',
                'route' => 'dashboardPage',
            ],
            $cons['transaksi_3'] => [
                'icon' => 'flaticon-arrows',
                'route' => 'orderOnlinePage'
            ]
        ];
    }

    public static function scheduleCalendarModal()
    {
        $appointment = mAppointment::with('patient')->get();
        $data = [
            'appointment' => $appointment
        ];

        $css = '
        <link href="' . asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') . '" rel="stylesheet"
          type="text/css"/>
          ';

        $js = '
    <script src="' . asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') . '"
            type="text/javascript"></script>
            ';

        $js .= view('scheduleCalendar/scheduleCalendar/scheduleCalendarJs', $data);

        $data = [
            'css' => $css,
            'js' => $js,
            'view' => view('scheduleCalendar/scheduleCalendar/scheduleCalendarModal')
        ];

        return $data;
    }

    public static function invoiceNumber()
    {
        $count = mPayment
            ::whereYear('invoice_date', '=', date('Y'))
            ->whereMonth('invoice_date', '=', date('m'))
            ->count();
        if ($count == 0) {
            $invoice_number = 1;
        } else {
            $invoice_number = mPayment
                    ::whereYear('invoice_date', '=', date('Y'))
                    ->whereMonth('invoice_date', '=', date('m'))
                    ->orderBy('invoice_number', 'DESC')
                    ->value('invoice_number') + 1;
        }

        return $invoice_number;
    }

    public static function invoiceLabel($invoice_number)
    {
        return 'INV-' . date('Ym-') . str_pad($invoice_number, 3, '0', STR_PAD_LEFT);
    }

    public static function invoice()
    {
        $count = mPembelian
            ::whereYear('pbl_tanggal_order', date('Y'))
            ->whereMonth('pbl_tanggal_order', date('m'))
            ->count();
        if ($count == 0) {
            $number = 1;
        } else {
            $pbl_no_faktur = mPembelian
                ::whereYear('pbl_tanggal_order', '=', date('Y'))
                ->whereMonth('pbl_tanggal_order', '=', date('m'))
                ->orderBy('id_pembelian', 'DESC')
                ->value('pbl_no_faktur');
            $number = substr($pbl_no_faktur, -3, 3);
        }

        $number = intval($number);

        return 'PBL-' . date('Ym-') . str_pad($number, 3, 0, STR_PAD_LEFT);
    }

    public static function fakturPembelian($kode_supplier)
    {
        $count = mPembelian
            ::whereYear('pbl_tanggal_order', date('Y'))
            ->whereMonth('pbl_tanggal_order', date('m'))
            ->count();
        if ($count == 0) {
            $number = 1;
        } else {
            $pbl_no_faktur = mPembelian
                ::whereYear('pbl_tanggal_order', '=', date('Y'))
                ->whereMonth('pbl_tanggal_order', '=', date('m'))
                ->orderBy('id_pembelian', 'DESC')
                ->value('pbl_no_faktur');

            $number = substr($pbl_no_faktur, -3, 3);
            $number = intval($number) + 1;
        }

        return 'PBL/' . $kode_supplier . '/' . date('Y') . '/' . date('m') . '/' . str_pad($number, 3, 0, STR_PAD_LEFT);

    }

    public static function fakturPenjualan($kode_pelanggan)
    {
        $count = mPenjualan
            ::whereYear('pjl_tanggal_penjualan', date('Y'))
            ->whereMonth('pjl_tanggal_penjualan', date('m'))
            ->count();
        if ($count == 0) {
            $number = 1;
        } else {
            $pjl_no_faktur = mPenjualan
                ::whereYear('pjl_tanggal_penjualan', '=', date('Y'))
                ->whereMonth('pjl_tanggal_penjualan', '=', date('m'))
                ->orderBy('id_penjualan', 'DESC')
                ->value('pjl_no_faktur');

            $number = substr($pjl_no_faktur, -3, 3);
            $number = intval($number) + 1;
        }

        return 'PJL/' . $kode_pelanggan . '/' . date('Y') . '/' . date('m') . '/' . str_pad($number, 3, 0, STR_PAD_LEFT);

    }

    public static function fakturHutangLain($kode_supplier)
    {
        $count = mHutangLain
            ::whereYear('hul_tanggal', date('Y'))
            ->whereMonth('hul_tanggal', date('m'))
            ->count();
        if ($count == 0) {
            $number = 1;
        } else {
            $hsp_no_faktur = mHutangLain
                ::whereYear('hul_tanggal', '=', date('Y'))
                ->whereMonth('hul_tanggal', '=', date('m'))
                ->orderBy('id_hutang_lain', 'DESC')
                ->value('hul_no_faktur');

            $number = substr($hsp_no_faktur, -3, 3);
            $number = intval($number) + 1;
        }

        return 'HUL/' . $kode_supplier . '/' . date('Y') . '/' . date('m') . '/' . str_pad($number, 3, 0, STR_PAD_LEFT);

    }

    public static function fakturHutangSupplier($kode_supplier)
    {
        $count = mHutangSupplier
            ::whereYear('hsp_tanggal', date('Y'))
            ->whereMonth('hsp_tanggal', date('m'))
            ->count();
        if ($count == 0) {
            $number = 1;
        } else {
            $hsp_no_faktur = mHutangSupplier
                ::whereYear('hsp_tanggal', '=', date('Y'))
                ->whereMonth('hsp_tanggal', '=', date('m'))
                ->orderBy('id_hutang_supplier', 'DESC')
                ->value('hsp_no_faktur');

            $number = substr($hsp_no_faktur, -3, 3);
            $number = intval($number) + 1;
        }

        return 'HSP/' . $kode_supplier . '/' . date('Y') . '/' . date('m') . '/' . str_pad($number, 3, 0, STR_PAD_LEFT);

    }

    public static function fakturPiutangPelanggan($kode_pelanggan)
    {
        $count = mPiutangPelanggan
            ::whereYear('ppl_tanggal', date('Y'))
            ->whereMonth('ppl_tanggal', date('m'))
            ->count();
        if ($count == 0) {
            $number = 1;
        } else {
            $ppl_no_faktur = mPiutangPelanggan
                ::whereYear('ppl_tanggal', '=', date('Y'))
                ->whereMonth('ppl_tanggal', '=', date('m'))
                ->orderBy('id_piutang_pelanggan', 'DESC')
                ->value('ppl_no_faktur');

            $number = substr($ppl_no_faktur, -3, 3);
            $number = intval($number) + 1;
        }

        return 'PPL/' . $kode_pelanggan . '/' . date('Y') . '/' . date('m') . '/' . str_pad($number, 3, 0, STR_PAD_LEFT);

    }

    public static function fakturPiutangLain($kode_pelanggan)
    {
        $count = mPiutangLain
            ::whereYear('ptl_tanggal', date('Y'))
            ->whereMonth('ptl_tanggal', date('m'))
            ->count();
        if ($count == 0) {
            $number = 1;
        } else {
            $ppl_no_faktur = mPiutangLain
                ::whereYear('ptl_tanggal', '=', date('Y'))
                ->whereMonth('ptl_tanggal', '=', date('m'))
                ->orderBy('id_piutang_lain', 'DESC')
                ->value('ptl_no_faktur');

            $number = substr($ppl_no_faktur, -3, 3);
            $number = intval($number) + 1;
        }

        return 'PTL/' . $kode_pelanggan . '/' . date('Y') . '/' . date('m') . '/' . str_pad($number, 3, 0, STR_PAD_LEFT);

    }

    public static function date_filter($request)
    {

        $date_first = mPembelian::orderBy('id_pembelian')->value('pbl_tanggal_order');

        $date_from = $request->date_from ? $request->date_from : date('01-m-Y');
        $date_to = $request->date_to ? $request->date_to : date("t-m-Y", strtotime($date_from));
        $date_from_db = date('Y-m-d', strtotime($date_from));
        $date_to_db = date('Y-m-d', strtotime($date_to));
        $date_where = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $data = [
            'date_first' => $date_first,
            'date_from' => $date_from,
            'date_to' => $date_to
        ];

        $date_filter = (string)\view('component/filterDate', $data);

        return [
            'date_from' => $date_from,
            'date_to' => $date_to,
            'date_from_db' => $date_from_db,
            'date_to_db' => $date_to_db,
            'date_first' => $date_first,
            'date_filter' => $date_filter,
            'date_where' => $date_where
        ];
    }

    public static function checkVarExist($var)
    {
        return isset($var) ? $var : '';
    }

    public static function day_format_id($day)
    {
        switch ($day) {
            case "Sunday":
                return 'Minggu';
                break;
            case "Monday":
                return 'Senin';
                break;
            case "Tuesday":
                return 'Selasa';
                break;
            case "Wednesday":
                return 'Rabu';
                break;
            case "Thursday":
                return 'Kamis';
                break;
            case "Friday":
                return 'Jumat';
                break;
            case "Saturday":
                return 'Sabtu';
                break;
        }
    }

    public static function whatsappSend($number, $message)
    {
//        $apikey = 604507;
//        $message = urlencode($message);
//        $client = new \GuzzleHttp\Client();
//        $request = $client->get('https://api.callmebot.com/whatsapp.php?phone='.$nomer.'&text='.$message.'&apikey='.$apikey);
//        $response = $request->getBody()->getContents();
//        echo '<pre>';
//        print_r($response);
//        exit;


//        $chatApiToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDk4MTM2NTcsInVzZXIiOiI2MjgxOTM0MzY0MDYzIn0.YEq5LzdGO1vpbGw6Xle9SJo_qFVs5WfM7AZmOTaO4rs"; // Get it from https://www.phphive.info/255/get-whatsapp-password/
//
////        $number = "+6281934364063"; // Number
////        $message = "Hello :)"; // Message
//
//        $curl = curl_init();
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => 'http://chat-api.phphive.info/message/send/text',
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => '',
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 0,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => 'POST',
//            CURLOPT_POSTFIELDS =>json_encode(array("jid"=> $number."@s.whatsapp.net", "message" => $message)),
//            CURLOPT_HTTPHEADER => array(
//                'Authorization: Bearer '.$chatApiToken,
//                'Content-Type: application/json'
//            ),
//        ));
//
//        $response = curl_exec($curl);
//        curl_close($curl);
//        echo $response;
//
//        echo $number.'<br  />';
//        echo $message;


        $apikey = 'ZIAWMPS5ZTVJLMGALBW3'; // api key nomer, rumah sunat bali
        $message = urlencode($message);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://panel.rapiwha.com/send_message.php?apikey=" . $apikey . "&number=" . $number . "&text=" . $message,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }


    }

    public static function appointment_status($appointment_status)
    {
        switch ($appointment_status) {
            case "prepare":
                return '<span class="m-badge m-badge--warning m-badge--wide">Dalam Proses</span>';
                break;
            case "send":
                return '<span class="m-badge m-badge--success m-badge--wide">Sudah Terkirim</span>';
                break;
            default:
                $appointment_status;
        }
    }

    public static function barang_golongan_label($brg_golongan)
    {
        switch ($brg_golongan) {
            case "luar":
                return 'Obat Luar';
                break;
            case "dalam":
                return 'Obat Dalam';
                break;
            default:
                return $brg_golongan;
        }
    }

    public static function greating()
    {
        $time = date("H");
        /* Set the $timezone variable to become the current timezone */
        $timezone = date("e");
        /* If the time is less than 1200 hours, show good morning */
        if ($time < "10") {
            return "Selamat Pagi";
        } else
            /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
            if ($time >= "10" && $time < "14") {
                return "Selamat Siang";
            } else
                /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
                if ($time >= "14" && $time < "19") {
                    return "Selamat Sore";
                } else
                    /* Finally, show good night if the time is greater than or equal to 1900 hours */
                    if ($time >= "19") {
                        return "Selamat Malam";
                    }
    }

}
