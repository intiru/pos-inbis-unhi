<?php

namespace app\Http\Controllers\PenyesuaianStok;

use app\Models\mArusStok;
use app\Models\mBarang;
use app\Models\mHistoryPenyesuaianStok;
use app\Models\mHutangSupplier;
use app\Models\mPelanggan;
use app\Models\mPembelian;
use app\Models\mPembelianDetail;
use app\Models\mPenjualan;
use app\Models\mPenjualanDetail;
use app\Models\mPiutangPelanggan;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use app\Models\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PenyesuaianStok extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $ppnPersen;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');

        $this->menuActive = $cons['penyesuaian_stok'];
        $this->ppnPersen = $cons_top['ppnPersen'];
        $this->breadcrumb = [
            [
                'label' => $cons['penyesuaian_stok'],
                'route' => route('penyesuaianStokList')
            ]
        ];
    }

    function index(Request $request)
    {
        $data = Main::data($this->breadcrumb);

        $filter_component = Main::date_filter($request);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "brg_kode"],
            ["data" => "brg_nama"],
            ["data" => "brg_golongan"],
            ["data" => "brg_minimal_stok"],
            ["data" => "total_stok"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db
            ),
        ]);

        return view('penyesuaianStok/penyesuaianStok/penyesuaianStokList', $data);
    }

    function data_table(Request $request)
    {

        $total_data = mBarang
            ::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_barang'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mBarang
            ::select([
                'id_barang',
                'brg_kode',
                'brg_nama',
                'brg_golongan',
                'brg_minimal_stok',
            ])
            ->withCount([
                'stok_barang AS total_stok' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(sbr_qty)'));
                }
            ])
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_barang = Main::encrypt($row->id_barang);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['brg_kode'] = $row->brg_kode;
            $nestedData['brg_nama'] = $row->brg_nama;
            $nestedData['brg_golongan'] = $row->brg_golongan;
            $nestedData['brg_minimal_stok'] = Main::format_number($row->brg_minimal_stok);
            $nestedData['total_stok'] = Main::format_number($row->total_stok);
            $nestedData['options'] = '
                <button 
                    type="button" 
                    class="btn m-btn--pill btn-info btn-sm btn-modal-general"
                    data-route="' . route('penyesuaianStokModal', ['id_barang' => $id_barang]) . '">
                    Stok Opname
                </button>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function penyesuaian_modal($id_barang)
    {
        $id_barang = Main::decrypt($id_barang);
        $stok_barang = mStokBarang
            ::with([
                'barang',
                'satuan',
                'supplier'
            ])
            ->where('id_barang', $id_barang)
            ->get();

        $data = [
            'stok_barang' => $stok_barang,
            'id_barang' => $id_barang
        ];

        return view('penyesuaianStok.penyesuaianStok.modalBarangStok', $data);
    }

    function update(Request $request)
    {
        $rules = [
            'hps_qty_akhir' => 'required',
            'hps_qty_akhir.*' => 'required'
        ];

        $attributes = [
            'hps_qty_akhir' => 'Stok Penyesesuaian'
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['hps_qty_akhir.' . $i] = 'Stok Penyesuaian ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }


        $id_barang = $request->input('id_barang');
        $user = Session::get('user');
        $id_user = $user['id'];
        $nama_barang_arr = $request->input('nama_barang');
        $id_stok_barang_arr = $request->input('id_stok_barang');
        $hps_qty_awal_arr = $request->input('hps_qty_awal');
        $hps_qty_akhir_arr = $request->input('hps_qty_akhir');
        $hps_keterangan_arr = $request->input('hps_keterangan');

        $keterangan_penyesuaian_tidak_terisi = FALSE;
        $errors = [];

        foreach ($id_stok_barang_arr as $index => $id_stok_barang) {

            $hps_qty_awal = $hps_qty_awal_arr[$index];
            $hps_qty_akhir = Main::format_number_db($hps_qty_akhir_arr[$index]);
            $hps_keterangan = $hps_keterangan_arr[$index];

            if ($hps_qty_awal != $hps_qty_akhir && !isset($hps_keterangan)) {
                $errors[] = [
                    'Isian <strong>Keterangan Penyesuaian Stok ke ' . ++$index . '</strong> wajib diisi, karena ada penyesuaian'
                ];
            }
        }

        if (count($errors) > 0) {
            $keterangan_penyesuaian_tidak_terisi = TRUE;
        }


        if ($keterangan_penyesuaian_tidak_terisi) {
            return response([
                'errors' => $errors
            ], 422);
        }


        DB::beginTransaction();
        try {

            foreach ($id_stok_barang_arr as $index => $id_stok_barang) {

                $stok_barang = mStokBarang::where('id_stok_barang', $id_stok_barang)->first();
                $hps_qty_awal = $hps_qty_awal_arr[$index];
                $hps_qty_akhir = Main::format_number_db($hps_qty_akhir_arr[$index]);
                $hps_keterangan = $hps_keterangan_arr[$index];

                /**
                 * Check apakah ada penyesuaian stok atau tidak dengan check qty sebelum dan sesudah
                 */

                if ($hps_qty_awal != $hps_qty_akhir) {

                    $asb_stok_awal = mStokBarang::where('id_stok_barang', $id_stok_barang)->value('sbr_qty');

                    $stok_barang_data = [
                        'sbr_qty' => $hps_qty_akhir
                    ];

                    mStokBarang::where('id_stok_barang', $id_stok_barang)->update($stok_barang_data);


                    /**
                     * Memasukkan ke history data penyesuaian stok
                     */

                    $history_penyesuaian_stok_data = [
                        'id_barang' => $id_barang,
                        'id_stok_barang' => $id_stok_barang,
                        'id_user' => $id_user,
                        'hps_qty_awal' => $hps_qty_awal,
                        'hps_qty_akhir' => $hps_qty_akhir,
                        'hps_keterangan' => $hps_keterangan,
                    ];

                    $history_penyesuaian_stok_response = mHistoryPenyesuaianStok::create($history_penyesuaian_stok_data);
                    $id_history_penyesuaian_stok = $history_penyesuaian_stok_response->id_history_penyesuaian_stok;

                    if ($hps_qty_awal > $hps_qty_akhir) {
                        $asb_stok_masuk = 0;
                        $asb_stok_keluar = $hps_qty_awal - $hps_qty_akhir;
                    } else {
                        $asb_stok_masuk = $hps_qty_akhir - $hps_qty_awal;
                        $asb_stok_keluar = 0;
                    }

                    $asb_stok_terakhir = $hps_qty_akhir;
                    $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');

                    $arus_stok_data = [
                        'id_pembelian' => $stok_barang->id_pembelian,
                        'id_pembelian_detail' => $stok_barang->id_pembelian_detail,
                        'id_barang' => $id_barang,
                        'id_stok_barang' => $id_stok_barang,
                        'id_history_penyesuaian_stok' => $id_history_penyesuaian_stok,
                        'id_user' => $id_user,
                        'asb_stok_awal' => $asb_stok_awal,
                        'asb_stok_masuk' => $asb_stok_masuk,
                        'asb_stok_keluar' => $asb_stok_keluar,
                        'asb_stok_terakhir' => $asb_stok_terakhir,
                        'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                        'asb_keterangan' => 'Penyesuaian Stok No. Batch '.$stok_barang->sbr_kode_batch.' tanggal '.date('d-M-Y H:i'),
                        'asb_tipe_proses' => 'update',
                        'asb_nama_proses' => 'penyesuaian_stok'
                    ];

                    mArusStok::create($arus_stok_data);

                }


            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }


    function index_history(Request $request)
    {
        $data = Main::data($this->breadcrumb, $this->menuActive);

        $filter_component = Main::date_filter($request);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];

        $datatable_column = [
            ["data" => "no"],
            ["data" => "created_at"],
            ["data" => "brg_kode"],
            ["data" => "brg_nama"],
            ["data" => "sbr_kode_batch"],
            ["data" => "stn_nama"],
            ["data" => "hps_qty_awal"],
            ["data" => "hps_qty_akhir"],
            ["data" => "hps_keterangan"],
            ["data" => "user"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db
            ),
        ]);

        return view('penyesuaianStok/penyesuaianStok/penyesuaianStokHistoryList', $data);
    }

    function data_table_history(Request $request)
    {
        $data_post = $request->input('data');
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mHistoryPenyesuaianStok
            ::whereBetween('created_at', $where_date)
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_history_penyesuaian_stok'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mHistoryPenyesuaianStok
            ::with([
                'barang',
                'stok_barang',
                'stok_barang.satuan',
                'user.karyawan'
            ])
            ->whereBetween('created_at', $where_date)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['created_at'] = $row->created_at;
            $nestedData['brg_kode'] = $row->barang->brg_kode;
            $nestedData['brg_nama'] = $row->barang->brg_nama;
            $nestedData['sbr_kode_batch'] = $row->stok_barang->sbr_kode_batch;
            $nestedData['stn_nama'] = $row->stok_barang->satuan->stn_nama;
            $nestedData['hps_qty_awal'] = Main::format_number($row->hps_qty_awal);
            $nestedData['hps_qty_akhir'] = Main::format_number($row->hps_qty_akhir);
            $nestedData['hps_keterangan'] = $row->hps_keterangan;
            $nestedData['user'] = $row->user->karyawan->nama_karyawan;


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

}
