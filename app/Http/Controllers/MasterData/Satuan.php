<?php

namespace app\Http\Controllers\MasterData;

use app\Models\mSatuan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;

class Satuan extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_satuan'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mSatuan
            ::orderBy('stn_nama', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('masterData/satuan/satuanList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'stn_nama' => 'required'
        ]);

        $data = $request->except('_token');
        mSatuan::create($data);
    }

    function edit_modal($id_satuan)
    {
        $id_satuan = Main::decrypt($id_satuan);
        $edit = mSatuan::where('id_satuan', $id_satuan)->first();
        $data = [
            'edit' => $edit
        ];

        return view('masterData/satuan/satuanEditModal', $data);
    }

    function delete($id_satuan)
    {
        $id_satuan = Main::decrypt($id_satuan);
        mSatuan::where('id_satuan', $id_satuan)->delete();
    }

    function update(Request $request, $id_satuan)
    {
        $id_satuan = Main::decrypt($id_satuan);
        $request->validate([
            'stn_nama' => 'required'
        ]);
        $data = $request->except("_token");
        mSatuan::where(['id_satuan' => $id_satuan])->update($data);
    }
}
