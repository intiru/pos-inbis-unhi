<?php

namespace app\Http\Controllers\MasterData;

use app\Models\mJenisBarang;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;

class JenisBarang extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['masterData'],
                'route' => ''
            ],
            [
                'label' => $cons['master_jenis_barang'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mJenisBarang
            ::orderBy('jbr_nama', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('masterData/jenisBarang/jenisBarangList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'jbr_nama' => 'required'
        ]);

        $data = $request->except('_token');
        mJenisBarang::create($data);
    }

    function edit_modal($id_jenis_barang)
    {
        $id_jenis_barang = Main::decrypt($id_jenis_barang);
        $edit = mJenisBarang::where('id_jenis_barang', $id_jenis_barang)->first();
        $data = [
            'edit' => $edit
        ];

        return view('masterData/jenisBarang/jenisBarangEditModal', $data);
    }

    function delete($id_jenis_barang)
    {
        $id_jenis_barang = Main::decrypt($id_jenis_barang);
        mJenisBarang::where('id_jenis_barang', $id_jenis_barang)->delete();
    }

    function update(Request $request, $id_jenis_barang)
    {
        $id_jenis_barang = Main::decrypt($id_jenis_barang);
        $request->validate([
            'jbr_nama' => 'required'
        ]);
        $data = $request->except("_token");
        mJenisBarang::where(['id_jenis_barang' => $id_jenis_barang])->update($data);
    }
}
