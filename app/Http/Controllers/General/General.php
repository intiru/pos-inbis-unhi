<?php

namespace app\Http\Controllers\General;

use app\Helpers\Main;
use app\Models\mCity;
use app\Models\mSubdistrict;
use Barryvdh\DomPDF\Facade as PDF;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;


class General extends Controller
{

    function city_list(Request $request)
    {
        $id_province = $request->id_province;

        $city = mCity::select(['id_city AS id', 'city_name AS text'])->where('id_province', $id_province)->orderBy('city_name', 'ASC')->get();

        $data_city = [];
        $data_city[] = [
            'id' => '',
            'text' => 'Pilih Kabupaten'
        ];

        foreach ($city as $row) {
            $data_city[] = $row;
        }


        return [
            'data' => $data_city
        ];

    }

    function subdistrict_list(Request $request)
    {
        $id_city = $request->id_city;

        $subdistrict = mSubdistrict::select(['id_subdistrict AS id', 'subdistrict_name AS text'])->where('id_city', $id_city)->orderBy('subdistrict_name', 'ASC')->get();
        $data_subdistrict = [];
        $data_subdistrict[] = [
            'id' => '',
            'text' => 'Pilih Kecamatan'
        ];


        return [
            'data' => $subdistrict
        ];

    }

    function ongkir(Request $request)
    {
        $jenis_kurir = $request->input('jenis_kurir');
        $id_subdistrict = $request->input('id_subdistrict');
        $weight = $request->input('weight');
        $data = [];
        $rajaongkir = [];

        if($jenis_kurir && $id_subdistrict && $weight) {

            $client = new Client(['base_uri' => 'https://pro.rajaongkir.com/']);
            $response_rajaongkir = $client->request('POST', '/api/cost', ['form_params' => [
                'key' => Main::$rajaongkir_key,
                'origin' => '1574',
                'originType' => 'subdistrict',
                'destination' => $id_subdistrict,
                'destinationType' => 'subdistrict',
                'weight' => $weight,
                'courier' => $jenis_kurir,
            ]]);

            $rajaongkir = json_decode($response_rajaongkir->getBody(), TRUE);
            $ongkir_list = $rajaongkir['rajaongkir']['results'][0]['costs'];

            foreach ($ongkir_list as $row) {
                $data[] = [
                    'id' => $row['cost'][0]['value'],
                    'text' => $row['description'] . ' - ' . Main::format_number($row['cost'][0]['value']) . ' - ' . $row['cost'][0]['etd'] . ' hari'
                ];
            }
        }

        return [
            'data' => $data,
            'rajaongkir' => $rajaongkir
        ];

    }

    function cetak_pdf(Request $request)
    {
        $view = $request->input('view');

        $data = [
            'view' => $view
        ];

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('general/tempPdf', $data);

        return $pdf
            ->setPaper('A4', 'portrait')
            ->stream('Payment Print');
    }

}
