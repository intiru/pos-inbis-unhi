<?php

namespace app\Http\Controllers\Pembelian;

use app\Models\mArusStok;
use app\Models\mBarang;
use app\Models\mHutangSupplier;
use app\Models\mPembelian;
use app\Models\mPembelianDetail;
use app\Models\mPiutangLain;
use app\Models\mSatuan;
use app\Models\mStokBarang;
use app\Models\mSupplier;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Pembelian extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $ppnPersen;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');
        $this->menuActive = $cons['pembelian'];
        $this->ppnPersen = $cons_top['ppnPersen'];
        $this->breadcrumb = [
            [
                'label' => $cons['pembelian'],
                'route' => route('pembelianList')
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];


        $data = Main::data($this->breadcrumb);
        $datatable_column = [
            ["data" => "no"],
            ["data" => "no_faktur"],
            ["data" => "tanggal_order"],
            ["data" => "jenis_pembayaran"],
            ["data" => "grand_total"],
            ["data" => "qty_total"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db
            ),
        ]);

        return view('pembelian/pembelian/pembelianList', $data);
    }


    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mPembelian
            ::whereBetween('pbl_tanggal_order', $where_date)
            ->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_pembelian'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mPembelian
            ::select([
                'id_pembelian',
                'pbl_no_faktur',
                'pbl_tanggal_order',
                'pbl_jenis_pembayaran',
                'pbl_grand_total',
            ])
            ->whereBetween('pbl_tanggal_order', $where_date)
            ->withCount([
                'pembelian_detail AS total_qty' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(pbd_qty)'));
                }
            ])
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_pembelian = Main::encrypt($row->id_pembelian);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['no_faktur'] = $row->pbl_no_faktur;
            $nestedData['tanggal_order'] = Main::format_datetime($row->pbl_tanggal_order);
            $nestedData['jenis_pembayaran'] = $row->pbl_jenis_pembayaran;
            $nestedData['grand_total'] = Main::format_number($row->pbl_grand_total);
            $nestedData['qty_total'] = Main::format_number($row->total_qty);
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_done dropdown-item"
                           href="' . route('pembelianEdit', ['id_pembelian' => $id_pembelian]) . '">
                            <i class="la la-edit"></i>
                            Edit
                        </a>
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('pembelianDetailModal', ['id_pembelian' => $id_pembelian]) . '">
                            <i class="la la-info"></i>
                            Detail
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all()
        );

        return $json_data;
    }

    function create()
    {
        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Pembelian Baru',
                    'route' => ''
                ]
            ]
        );
        $data = Main::data($breadcrumb, $this->menuActive);
        $supplier = mSupplier::orderBy('spl_nama', 'ASC')->get();
        $barang = mBarang::orderBy('brg_kode')->get();
        $satuan = mSatuan::orderBy('stn_nama', 'ASC')->get();

        $data = array_merge(
            $data, array(
                'supplier' => $supplier,
                'barang' => $barang,
                'satuan' => $satuan
            )
        );

        return view('pembelian/pembelian/pembelianCreate', $data);
    }

    function no_faktur(Request $request)
    {
        $kode_supplier = $request->input('kode_supplier');
        $id_supplier = $request->input('id_supplier');
        $pbl_no_faktur = Main::fakturPembelian($kode_supplier);


        return [
            $pbl_no_faktur
        ];
    }

    function insert(Request $request)
    {
        $rules = [
            'id_supplier' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'kode_batch' => 'required',
            'kode_batch.*' => 'required',
            'expired_date' => 'required',
            'expired_date.*' => 'required',
            'harga_beli' => 'required',
            'harga_beli.*' => 'required',
            'harga_jual' => 'required',
            'harga_jual.*' => 'required',
            'qty' => 'required',
            'qty.*' => 'required',
            'biaya_tambahan' => 'required',
            'potongan' => 'required',
            'jumlah_bayar' => 'required',
        ];

        $attributes = [
            'id_supplier' => 'Supplier',
            'id_barang' => 'Barang',
            'kode_batch' => 'Kode Batch',
            'expired_date' => 'Expired Date',
            'harga_beli' => 'Harga Beli',
            'harga_jual' => 'Harga Jual',
            'qty' => 'Qty Barang',
            'biaya_tambahan' => 'Biaya Tambahan',
            'potongan' => 'Potongan',
            'jumlah_bayar' => 'Jumlah Bayar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['kode_batch.' . $i] = 'Kode Batch ke-' . $next;
            $attr['expired_date.' . $i] = 'Expired Date ke-' . $next;
            $attr['harga_beli.' . $i] = 'Harga Beli ke-' . $next;
            $attr['harga_jual.' . $i] = 'Harga Jual ke-' . $next;
            $attr['qty.' . $i] = 'Qty Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_supplier = $request->input('id_supplier');
        $kode_supplier = $request->input('kode_supplier');
        $id_barang_arr = $request->input('id_barang');

        $pbl_no_faktur = $request->input('no_faktur');
        $pbl_tanggal_order = Main::format_datetime_db($request->input('tanggal'));
        $pbl_jenis_pembayaran = $request->input('pembayaran');
        $pbl_keterangan = $request->input('keterangan');

        $pbd_harga_net_arr = $request->input('harga_net');
        $pbd_sub_total_arr = $request->input('sub_total');
        $pbd_harga_beli_arr = $request->input('harga_beli');
        $pbd_harga_jual_arr = $request->input('harga_jual');
        $pbd_kode_batch_arr = $request->input('kode_batch');
        $pbd_konsinyasi_status_arr = $request->input('konsinyasi');
        $pbd_expired_date_arr = $request->input('expired_date');
        $id_satuan_arr = $request->input('id_satuan');
        $pbd_ppn_nominal_arr = $request->input('ppn_nominal');
        $pbd_ppn_persen = $this->ppnPersen;
        $pbd_qty_arr = $request->input('qty');

        $pbl_total = $request->input('total');
        $pbl_biaya_tambahan = Main::format_number_db($request->input('biaya_tambahan'));
        $pbl_potongan = Main::format_number_db($request->input('potongan'));
        $pbl_grand_total = $request->input('grand_total');
        $pbl_jumlah_bayar = Main::format_number_db($request->input('jumlah_bayar'));
        $pbl_sisa_pembayaran = $request->input('sisa_pembayaran');

        $hsp_tanggal_jatuh_tempo = Main::format_date_db($request->input('tanggal_jatuh_tempo'));

        $created_at = date('Y-m-d H:i:s');
        $updated_at = $created_at;


        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke pembelian data
             */
            $pembelian_data = [
                'id_supplier' => $id_supplier,
                'id_user' => $id_user,
                'pbl_no_faktur' => $pbl_no_faktur,
                'pbl_tanggal_order' => $pbl_tanggal_order,
                'pbl_jenis_pembayaran' => $pbl_jenis_pembayaran,
                'pbl_total' => $pbl_total,
                'pbl_biaya_tambahan' => $pbl_biaya_tambahan,
                'pbl_potongan' => $pbl_potongan,
                'pbl_grand_total' => $pbl_grand_total,
                'pbl_jumlah_bayar' => $pbl_jumlah_bayar,
                'pbl_sisa_pembayaran' => $pbl_sisa_pembayaran,
                'pbl_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                'pbl_keterangan' => $pbl_keterangan,
            ];

            $response_pembelian = mPembelian::create($pembelian_data);
            $id_pembelian = $response_pembelian->id_pembelian;

            foreach ($id_barang_arr as $index => $id_barang) {

                /**
                 * Memasukkan data ke pembelian detail
                 */
                $pbd_kode_batch = $pbd_kode_batch_arr[$index];
                $pbd_harga_beli = Main::format_number_db($pbd_harga_beli_arr[$index]);
                $pbd_harga_jual = Main::format_number_db($pbd_harga_jual_arr[$index]);
                $pbd_expired_date = Main::format_date_db($pbd_expired_date_arr[$index]);
                $id_satuan = $id_satuan_arr[$index];
                $pbd_ppn_nominal = Main::format_number_db($pbd_ppn_nominal_arr[$index]);
                $pbd_harga_net = Main::format_number_db($pbd_harga_net_arr[$index]);
                $pbd_qty = Main::format_number_db($pbd_qty_arr[$index]);
                $pbd_sub_total = $pbd_sub_total_arr[$index];
                $pbd_konsinyasi_status = $pbd_konsinyasi_status_arr[$index];

                $pembelian_detail_data = [
                    'id_pembelian' => $id_pembelian,
                    'id_barang' => $id_barang,
                    'id_satuan' => $id_satuan,
                    'pbd_kode_batch' => $pbd_kode_batch,
                    'pbd_harga_beli' => $pbd_harga_beli,
                    'pbd_harga_jual' => $pbd_harga_jual,
                    'pbd_expired' => $pbd_expired_date,
                    'pbd_ppn_persen' => $pbd_ppn_persen,
                    'pbd_ppn_nominal' => $pbd_ppn_nominal,
                    'pbd_harga_net' => $pbd_harga_net,
                    'pbd_qty' => $pbd_qty,
                    'pbd_sub_total' => $pbd_sub_total,
                    'pbd_konsinyasi_status' => $pbd_konsinyasi_status,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at
                ];

                $response_pembelian_detail = mPembelianDetail::create($pembelian_detail_data);
                $id_pembelian_detail = $response_pembelian_detail->id_pembelian_detail;

                /**
                 * Memasukkan ke stok barang
                 */
                $stok_barang_data = [
                    'id_barang' => $id_barang,
                    'id_satuan' => $id_satuan,
                    'id_pembelian' => $id_pembelian,
                    'id_pembelian_detail' => $id_pembelian_detail,
                    'id_supplier' => $id_supplier,
                    'sbr_kode_batch' => $pbd_kode_batch,
                    'sbr_expired' => $pbd_expired_date,
                    'sbr_qty' => $pbd_qty,
                    'sbr_harga_beli' => $pbd_harga_beli,
                    'sbr_harga_jual' => $pbd_harga_jual,
                    'sbr_status' => 'ready',
                    'sbr_konsinyasi_status' => $pbd_konsinyasi_status
                ];

                $response_stok_barang = mStokBarang::create($stok_barang_data);
                $id_stok_barang = $response_stok_barang->id_stok_barang;
                $asb_stok_terakhir = $pbd_qty;
                $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');

                $arus_stok_data = [
                    'id_pembelian' => $id_pembelian,
                    'id_pembelian_detail' => $id_pembelian_detail,
                    'id_barang' => $id_barang,
                    'id_stok_barang' => $id_stok_barang,
                    'id_user' => $id_user,
                    'asb_stok_awal' => 0,
                    'asb_stok_masuk' => $pbd_qty,
                    'asb_stok_keluar' => 0,
                    'asb_stok_terakhir' => $asb_stok_terakhir,
                    'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                    'asb_keterangan' => 'Pembelian Barang No Faktur : ' . $pbl_no_faktur,
                    'asb_tipe_proses' => 'insert',
                    'asb_nama_proses' => 'pembelian'
                ];

                mArusStok::create($arus_stok_data);
            }

            if ($pbl_sisa_pembayaran > 0) {
                $hsp_no_faktur = Main::fakturHutangSupplier($kode_supplier);

                $hutang_supplier_data = [
                    'id_supplier' => $id_supplier,
                    'id_pembelian' => $id_pembelian,
                    'hsp_no_faktur' => $hsp_no_faktur,
                    'hsp_tanggal' => $pbl_tanggal_order,
                    'hsp_tanggal_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                    'hsp_total' => $pbl_sisa_pembayaran,
                    'hsp_sisa' => $pbl_sisa_pembayaran,
                    'hsp_keterangan' => 'Hutang Supplier #' . $kode_supplier . ' dengan no faktur ' . $pbl_no_faktur,
                    'hsp_status' => 'belum_lunas'
                ];

                mHutangSupplier::create($hutang_supplier_data);
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function edit($id_pembelian)
    {
        $id_pembelian = Main::decrypt($id_pembelian);

        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Edit Pembelian',
                    'route' => ''
                ]
            ]
        );

        $data = Main::data($breadcrumb, $this->menuActive);
        $supplier = mSupplier::orderBy('spl_nama', 'ASC')->get();
        $barang = mBarang::orderBy('brg_kode')->get();
        $satuan = mSatuan::orderBy('stn_nama', 'ASC')->get();
        $edit = mPembelian
            ::with([
                'supplier',
                'pembelian_detail',
                'pembelian_detail.barang',
            ])
            ->where('id_pembelian', $id_pembelian)
            ->first();

        $data = array_merge(
            $data, array(
                'supplier' => $supplier,
                'barang' => $barang,
                'satuan' => $satuan,
                'edit' => $edit
            )
        );

        return view('pembelian/pembelian/pembelianEdit', $data);
    }

    function update(Request $request, $id_pembelian)
    {
        $rules = [
            'kode_batch' => 'required',
            'kode_batch.*' => 'required',
            'expired_date' => 'required',
            'expired_date.*' => 'required',
            'harga_beli' => 'required',
            'harga_beli.*' => 'required',
            'harga_jual' => 'required',
            'harga_jual.*' => 'required',
            'biaya_tambahan' => 'required',
            'potongan' => 'required',
            'jumlah_bayar' => 'required',
        ];

        $attributes = [
            'kode_batch' => 'Kode Batch',
            'expired_date' => 'Expired Date',
            'harga_beli' => 'Harga Beli',
            'harga_jual' => 'Harga Jual',
            'biaya_tambahan' => 'Biaya Tambahan',
            'potongan' => 'Potongan',
            'jumlah_bayar' => 'Jumlah Bayar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['kode_batch.' . $i] = 'Kode Batch ke-' . $next;
            $attr['expired_date.' . $i] = 'Expired Date ke-' . $next;
            $attr['harga_beli.' . $i] = 'Harga Beli ke-' . $next;
            $attr['harga_jual.' . $i] = 'Harga Jual ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_pembelian = Main::decrypt($id_pembelian);
        $id_pembelian_detail_arr = $request->input('id_pembelian_detail');
        $id_supplier = $request->input('id_supplier');
        $kode_supplier = $request->input('kode_supplier');

        $pbl_tanggal_order = Main::format_datetime_db($request->input('tanggal'));
        $pbl_jenis_pembayaran = $request->input('pembayaran');
        $pbl_keterangan = $request->input('keterangan');

        $pbd_harga_net_arr = $request->input('harga_net');
        $pbd_sub_total_arr = $request->input('sub_total');
        $pbd_harga_beli_arr = $request->input('harga_beli');
        $pbd_harga_jual_arr = $request->input('harga_jual');
        $pbd_kode_batch_arr = $request->input('kode_batch');
        $pbd_konsinyasi_status_arr = $request->input('konsinyasi');
        $pbd_expired_date_arr = $request->input('expired_date');
        $id_satuan_arr = $request->input('id_satuan');
        $pbd_ppn_nominal_arr = $request->input('ppn_nominal');
        $pbd_ppn_persen = $this->ppnPersen;
        $pbd_qty_arr = $request->input('qty');

        $pbl_total = $request->input('total');
        $pbl_biaya_tambahan = Main::format_number_db($request->input('biaya_tambahan'));
        $pbl_potongan = Main::format_number_db($request->input('potongan'));
        $pbl_grand_total = $request->input('grand_total');
        $pbl_jumlah_bayar = Main::format_number_db($request->input('jumlah_bayar'));
        $pbl_sisa_pembayaran = $request->input('sisa_pembayaran');

        $hsp_tanggal_jatuh_tempo = Main::format_date_db($request->input('tanggal_jatuh_tempo'));

        $hutang_supplier = mHutangSupplier::where('id_pembelian', $id_pembelian)->first();
        $pembelian = mPembelian::where('id_pembelian', $id_pembelian)->first();


        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke pembelian data
             */
            $pembelian_data = [
                'pbl_tanggal_order' => $pbl_tanggal_order,
                'pbl_jenis_pembayaran' => $pbl_jenis_pembayaran,
                'pbl_total' => $pbl_total,
                'pbl_biaya_tambahan' => $pbl_biaya_tambahan,
                'pbl_potongan' => $pbl_potongan,
                'pbl_grand_total' => $pbl_grand_total,
                'pbl_jumlah_bayar' => $pbl_jumlah_bayar,
                'pbl_sisa_pembayaran' => $pbl_sisa_pembayaran,
                'pbl_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                'pbl_keterangan' => $pbl_keterangan,
            ];

            mPembelian::where('id_pembelian', $id_pembelian)->update($pembelian_data);

            foreach ($id_pembelian_detail_arr as $index => $id_pembelian_detail) {

                /**
                 * Memperbarui data ke pembelian detail
                 */
                $pbd_kode_batch = $pbd_kode_batch_arr[$index];
                $pbd_harga_beli = Main::format_number_db($pbd_harga_beli_arr[$index]);
                $pbd_harga_jual = Main::format_number_db($pbd_harga_jual_arr[$index]);
                $pbd_expired_date = Main::format_date_db($pbd_expired_date_arr[$index]);
                $id_satuan = $id_satuan_arr[$index];
                $pbd_ppn_nominal = Main::format_number_db($pbd_ppn_nominal_arr[$index]);
                $pbd_harga_net = Main::format_number_db($pbd_harga_net_arr[$index]);
                $pbd_sub_total = $pbd_sub_total_arr[$index];
                $pbd_konsinyasi_status = $pbd_konsinyasi_status_arr[$index];

                $pembelian_detail_data = [
                    'id_satuan' => $id_satuan,
                    'pbd_kode_batch' => $pbd_kode_batch,
                    'pbd_harga_beli' => $pbd_harga_beli,
                    'pbd_harga_jual' => $pbd_harga_jual,
                    'pbd_expired' => $pbd_expired_date,
                    'pbd_ppn_persen' => $pbd_ppn_persen,
                    'pbd_ppn_nominal' => $pbd_ppn_nominal,
                    'pbd_harga_net' => $pbd_harga_net,
                    'pbd_sub_total' => $pbd_sub_total,
                    'pbd_konsinyasi_status' => $pbd_konsinyasi_status,
                ];

                mPembelianDetail::where('id_pembelian_detail', $id_pembelian_detail)->update($pembelian_detail_data);

                /**
                 * Memperbarui data stok barang
                 */
                $stok_barang_data = [
                    'id_satuan' => $id_satuan,
                    'sbr_kode_batch' => $pbd_kode_batch,
                    'sbr_expired' => $pbd_expired_date,
                    'sbr_harga_beli' => $pbd_harga_beli,
                    'sbr_harga_jual' => $pbd_harga_jual,
                    'sbr_status' => 'ready',
                    'sbr_konsinyasi_status' => $pbd_konsinyasi_status
                ];

                mStokBarang
                    ::where('id_pembelian_detail', $id_pembelian_detail)
                    ->update($stok_barang_data);
            }

            /**
             * Jika sisa pembayaran pembelian dengan hutang supplier berbeda, karena ada perubahan harga pembelian barang
             */
            if ($pbl_sisa_pembayaran !== $hutang_supplier->hsp_total) {

                /**
                 * Jika sisa pembayaran pembelian lebih besar dari total hutang supplier,
                 * Maka, total, status, dan sisa hutang supplier diperbarui
                 */
                if (
                    $pbl_sisa_pembayaran > $hutang_supplier->hsp_total
                    && $hutang_supplier->hsp_status == 'belum_lunas') {

                    $hsp_sisa = ($pbl_sisa_pembayaran - $hutang_supplier->hsp_total) + $hutang_supplier->hsp_sisa;

                    $hutang_supplier_data = [
                        'hsp_total' => $pbl_sisa_pembayaran,
                        'hsp_status' => 'belum_lunas',
                        'hsp_tanggal_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                        'hsp_sisa' => $hsp_sisa
                    ];

                    mHutangSupplier::where('id_pembelian', $id_pembelian)->update($hutang_supplier_data);

                    /**
                     * Jika sisa pembayaran lebih kecil dari hutang supplier,
                     * maka pembaruan total, tanggal, dan sisa pembayaran
                     */
                } else if (
                    $pbl_sisa_pembayaran < $hutang_supplier->hsp_total
                    && $hutang_supplier->hsp_status == 'belum_lunas') {

                    $hutang_supplier_data = [
                        'hsp_total' => $pbl_sisa_pembayaran,
                        'hsp_tanggal_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                        'hsp_sisa' => $pbl_sisa_pembayaran
                    ];

                    mHutangSupplier::where('id_pembelian', $id_pembelian)->update($hutang_supplier_data);

                    /**
                     * Jika sisa pembayaran pembelian kurang dari total hutang supplier sebelumnya dan statusnya sudah lunas,
                     * maka kelebihan pembayaran tersebut akan menjadi piutang lain,
                     * dengan total piutang lain yaitu total hutang supplier - sisa pembayaran
                     */
                } else if (
                    $pbl_sisa_pembayaran < $hutang_supplier->hsp_total
                    && $hutang_supplier->hsp_status == 'lunas') {

                    /**
                     * Pembaruan hutang supplier
                     */

                    $hsp_total = $pbl_sisa_pembayaran < 0 ? 0 : $pbl_sisa_pembayaran;

                    $hutang_supplier_data = [
                        'hsp_total' => $hsp_total,
                        'hsp_tanggal_jatuh_tempo' => $hsp_tanggal_jatuh_tempo,
                    ];

                    mHutangSupplier::where('id_pembelian', $id_pembelian)->update($hutang_supplier_data);

                    /**
                     * Create piutang lain dari kelebihan bayar hutang supplier
                     */
                    $ptl_no_faktur = Main::fakturPiutangLain($kode_supplier);
                    $ptl_tanggal = date('Y-m-d');
                    $ptl_jatuh_tempo = date('Y-m-d');
                    $ptl_total = $hutang_supplier->hsp_total - $pbl_sisa_pembayaran;
                    $ptl_sisa = 0;
                    $ptl_status = 'belum_lunas';
                    $ptl_keterangan = 'Piutang Supplier karena pembaruan data Pembelian yang hutang suppllier sudah lunas, faktur pembelian ' . $pembelian->pbl_no_faktur . ' & faktur hutang supplier ' . $hutang_supplier->hsp_no_faktur;

                    $piutang_lain_data = [
                        'id_pembelian' => $id_pembelian,
                        'ptl_no_faktur' => $ptl_no_faktur,
                        'ptl_tanggal' => $ptl_tanggal,
                        'ptl_jatuh_tempo' => $ptl_jatuh_tempo,
                        'ptl_total' => $ptl_total,
                        'ptl_sisa' => $ptl_sisa,
                        'ptl_status' => $ptl_status,
                        'ptl_keterangan' => $ptl_keterangan
                    ];

                    mPiutangLain::create($piutang_lain_data);

                }
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function detail_modal($id_pembelian)
    {
        $id_pembelian = Main::decrypt($id_pembelian);
        $pembelian = mPembelian
            ::with([
                'supplier',
                'user',
                'user.karyawan',
                'pembelian_detail',
                'pembelian_detail.barang',
                'pembelian_detail.satuan',
            ])
            ->where('id_pembelian', $id_pembelian)
            ->first();

        $data = [
            'pembelian' => $pembelian,
        ];

        return view('pembelian.pembelian.pembelianDetailModal', $data);
    }
}
