<?php

namespace app\Http\Controllers\Midtrans;

use app\Models\mArusStok;
use app\Models\mConfig;
use app\Models\mHutangSupplier;
use app\Models\mKaryawan;
use app\Models\mNotif;
use app\Models\mPembelian;
use app\Models\mPembelianDetail;
use app\Models\mPenjualan;
use app\Models\mPenjualanDetail;
use app\Models\mPiutangPelanggan;
use app\Models\mStokBarang;
use app\Models\mUser;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class Notif extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $ppnPersen;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');

        $this->menuActive = $cons['laporan'];
        $this->ppnPersen = $cons_top['ppnPersen'];
        $this->breadcrumb = [
            [
                'label' => $cons['laporan'],
                'route' => route('laporanList')
            ]
        ];
    }

    function payment(Request $request)
    {

        DB::beginTransaction();
        try {

            $transaction_status = $request->input('transaction_status');
            $order_id = $request->input('order_id');

            if ($transaction_status == 'settlement') {

                $penjualan = mPenjualan
                    ::with([
                        'penjualan_detail',
                        'penjualan_detail.stok_barang',
                    ])
                    ->where('pjl_no_faktur', $order_id)
                    ->first();
                $id_user = $penjualan->id_user;

                if ($penjualan->pjl_status == 'belum_lunas') {

                    /**
                     * Update status penjualan ke lunas
                     */
                    mPenjualan
                        ::where('pjl_no_faktur', $order_id)
                        ->update([
                            'pjl_status' => 'lunas'
                        ]);

                    /**
                     * Update stok barang
                     * Mengurangi dari stok barang
                     *
                     **/

                    foreach ($penjualan->penjualan_detail as $row) {
                        $id_stok_barang = $row->id_stok_barang;
                        $pjd_qty = $row->pjd_qty;
                        $id_barang = $row->id_barang;
                        $id_penjualan = $row->id_penjualan;
                        $id_penjualan_detail = $row->id_penjualan_detail;
                        $pjl_no_faktur = $order_id;

                        /**
                         * Proses Pengurangan Stok Barang
                         */
                        $sbr_qty_now = mStokBarang::where('id_stok_barang', $id_stok_barang)->value('sbr_qty');
                        $asb_stok_terakhir = $sbr_qty_now - $pjd_qty;
                        $stok_barang_update = [
                            'sbr_qty' => $asb_stok_terakhir
                        ];
                        mStokBarang::where('id_stok_barang', $id_stok_barang)->update($stok_barang_update);
                        $asb_stok_total_terakhir = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');

                        /**
                         * Proses memasukkan data ke arus stok
                         */
                        $arus_stok_data = [
                            'id_penjualan' => $id_penjualan,
                            'id_penjualan_detail' => $id_penjualan_detail,
                            'id_barang' => $id_barang,
                            'id_stok_barang' => $id_stok_barang,
                            'id_user' => $id_user,
                            'asb_stok_awal' => $sbr_qty_now,
                            'asb_stok_masuk' => 0,
                            'asb_stok_keluar' => $pjd_qty,
                            'asb_stok_terakhir' => $asb_stok_terakhir,
                            'asb_stok_total_terakhir' => $asb_stok_total_terakhir,
                            'asb_keterangan' => 'Penjualan Barang No Faktur : ' . $pjl_no_faktur,
                            'asb_tipe_proses' => 'update',
                            'asb_nama_proses' => 'penjualan'
                        ];

                        mArusStok::create($arus_stok_data);

                    }


                    /**
                     * Proses Memasukkan Saldo ke setiap user
                     */

                    $penjualan_total = $penjualan->pjl_total;
                    $jumlah_komisi_tenant_dengan_inbis_dan_marketer = $penjualan_total * Main::$komisi_penjual_inbis_dan_marketer;

                    $komisi_inbis = $jumlah_komisi_tenant_dengan_inbis_dan_marketer / 2;
                    $komisi_marketer = $jumlah_komisi_tenant_dengan_inbis_dan_marketer / 2;

                    /**
                     * Tambah saldo marketer
                     */
                    $id_karyawan = mUser::where('id', $id_user)->value('id_karyawan');
                    $saldo_marketer_before = mKaryawan::where('id', $id_karyawan)->value('saldo_karyawan');
                    $saldo_marketer_now = $saldo_marketer_before + $komisi_marketer;
                    mKaryawan::where('id', $id_karyawan)->update(['saldo_karyawan' => $saldo_marketer_now]);

                    /**
                     * Tambah saldo inbis
                     */

                    $saldo_inbis_before = mConfig::where('variable', 'saldo_komisi_inbis')->value('value');
                    $saldo_inbis_now = $saldo_inbis_before + $komisi_inbis;
                    mConfig::where('variable', 'saldo_komisi_inbis')->update(['value' => $saldo_inbis_now]);

                }
            }

            /**
             * Dump callback
             */
            $json = json_encode($request->all());
            $data_insert = [
                'json' => $json,
                'tipe' => 'payment'
            ];

            mNotif::create($data_insert);

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }


    }

    function recurring(Request $request)
    {
        $json = json_encode($request->all());

        $data_insert = [
            'json' => $json,
            'tipe' => 'recurring'
        ];

        mNotif::create($data_insert);
    }

    function pay_account(Request $request)
    {
        $json = json_encode($request->all());

        $data_insert = [
            'json' => $json,
            'tipe' => 'pay_account'
        ];

        mNotif::create($data_insert);
    }

    function finish()
    {
        return view('midtrans/page');
    }

    function unfinish()
    {
        return view('midtrans/page');
    }

    function error()
    {
        return view('midtrans/page');
    }

}
