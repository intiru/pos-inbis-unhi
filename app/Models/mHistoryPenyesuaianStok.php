<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mHistoryPenyesuaianStok extends Model
{
    use SoftDeletes;

    protected $table = 'history_penyesuaian_stok';
    protected $primaryKey = 'id_history_penyesuaian_stok';
    protected $fillable = [
        'id_barang',
        'id_stok_barang',
        'id_user',
        'hps_qty_awal',
        'hps_qty_akhir',
        'hps_keterangan'
    ];

    public function barang()
    {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function stok_barang()
    {
        return $this->belongsTo(mStokBarang::class, 'id_stok_barang');
    }

    public function user()
    {
        return $this->belongsTo(mUser::class, 'id_user');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
