<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mUser extends Model
{
    use SoftDeletes;

    protected $table = 'tb_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_karyawan',
        'id_user_role',
        'username',
        'password',
        'inisial_karyawan',
        'id_lokasi'
    ];
    protected $hidden = [
        'password'
    ];

    function karyawan()
    {
        return $this->belongsTo(mKaryawan::class, 'id_karyawan');
    }

    function user_role()
    {
        return $this->belongsTo(mUserRole::class, 'id_user_role');
    }
}
