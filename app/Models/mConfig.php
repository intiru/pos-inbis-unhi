<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mConfig extends Model
{
    use SoftDeletes;

    protected $table = 'config';
    protected $primaryKey = 'id_config';
    protected $fillable = [
        'variable',
        'value',
    ];
}
