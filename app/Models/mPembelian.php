<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPembelian extends Model
{
    use SoftDeletes;

    protected $table = 'pembelian';
    protected $primaryKey = 'id_pembelian';
    protected $fillable = [
        'id_supplier',
        'id_user',
        'pbl_urutan',
        'pbl_no_faktur',
        'pbl_tanggal_order',
        'pbl_jenis_pembayaran',
        'pbl_keterangan',
        'pbl_total',
        'pbl_biaya_tambahan',
        'pbl_potongan',
        'pbl_grand_total',
        'pbl_status',
        'pbl_jumlah_bayar',
        'pbl_sisa_pembayaran',
        'pbl_jatuh_tempo'
    ];

    public function supplier() {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    public function user() {
        return $this->belongsTo(mUser::class, 'id_user');
    }

    public function pembelian_detail() {
        return $this->hasMany(mPembelianDetail::class, 'id_pembelian');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
