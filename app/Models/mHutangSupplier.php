<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mHutangSupplier extends Model
{
    use SoftDeletes;

    protected $table = 'hutang_supplier';
    protected $primaryKey = 'id_hutang_supplier';
    protected $fillable = [
        'id_supplier',
        'id_pembelian',
        'hsp_no_faktur',
        'hsp_tanggal',
        'hsp_tanggal_jatuh_tempo',
        'hsp_total',
        'hsp_sisa',
        'hsp_keterangan',
        'hsp_status',
    ];

    public function supplier()
    {
        return $this->belongsTo(mSupplier::class, 'id_supplier');
    }

    public function pembelian()
    {
        return $this->belongsTo(mPembelian::class, 'id_pembelian');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
