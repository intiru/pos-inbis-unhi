<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class mNotif extends Model
{
    use SoftDeletes;

    protected $table = 'notif';
    protected $primaryKey = 'id_notif';
    protected $fillable = [
        'json',
        'tipe',
    ];
}
