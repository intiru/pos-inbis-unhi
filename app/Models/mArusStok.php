<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mArusStok extends Model
{
    use SoftDeletes;

    protected $table = 'arus_stok';
    protected $primaryKey = 'id_arus_stok_barang';
    protected $fillable = [
        'id_pembelian',
        'id_pembelian_detail',
        'id_penjualan',
        'id_penjualan_detail',
        'id_barang',
        'id_stok_barang',
        'id_history_penyesuaian_stok',
        'id_user',
        'asb_stok_awal',
        'asb_stok_masuk',
        'asb_stok_keluar',
        'asb_stok_terakhir',
        'asb_stok_total_terakhir',
        'asb_keterangan',
        'asb_tipe_proses',
        'asb_nama_proses',
    ];

    public function barang()
    {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function stok_barang() {
        return $this->belongsTo(mStokBarang::class, 'id_stok_barang');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
