<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mSupplier extends Model
{
    use SoftDeletes;

    protected $table = 'supplier';
    protected $primaryKey = 'id_supplier';
    protected $fillable = [
        'id_supplier',
        'spl_kode',
        'spl_nama',
        'spl_alamat',
        'spl_phone',
        'spl_email',
        'spl_keterangan',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
