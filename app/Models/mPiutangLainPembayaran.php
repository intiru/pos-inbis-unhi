<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPiutangLainPembayaran extends Model
{
    use SoftDeletes;

    protected $table = 'piutang_lain_pembayaran';
    protected $primaryKey = 'id_piutang_lain_pembayaran';
    protected $fillable = [
        'id_piutang_lain',
        'id_user',
        'plp_total_piutang',
        'plp_jumlah_bayar',
        'plp_sisa_bayar',
        'plp_tanggal_bayar',
        'plp_keterangan',
    ];

    public function user() {
        return $this->belongsTo(mUser::class, 'id_user');
    }

    public function piutang_lain() {
        return $this->belongsTo(mPiutangLain::class, 'id_piutang_lain');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
