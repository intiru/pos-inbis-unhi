<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mHutangSupplierPembayaran extends Model
{
    use SoftDeletes;

    protected $table = 'hutang_supplier_pembayaran';
    protected $primaryKey = 'id_hutang_supplier_pembayaran';
    protected $fillable = [
        'id_hutang_supplier',
        'id_user',
        'hsp_total_hutang',
        'hsp_jumlah_bayar',
        'hsp_sisa_bayar',
        'hsp_tanggal_bayar',
        'hsp_keterangan',
    ];

    public function user() {
        return $this->belongsTo(mUser::class, 'id_user');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
