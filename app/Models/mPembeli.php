<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPembeli extends Model
{
    use SoftDeletes;

    protected $table = 'pembeli';
    protected $primaryKey = 'id_pembeli';
    protected $fillable = [
        'id_penjualan',
        'id_province',
        'id_city',
        'id_subdistrict',
        'pbl_nama',
        'pbl_phone',
        'pbl_alamat_tinggal',
    ];

    public function penjualan() {
        return $this->belongsTo(mPenjualan::class, 'id_penjualan');
    }

    public function province() {
        return $this->belongsTo(mProvince::class, 'id_province');
    }

    public function city() {
        return $this->belongsTo(mCity::class, 'id_city');
    }

    public function subdistrict() {
        return $this->belongsTo(mSubdistrict::class, 'id_subdistrict');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
