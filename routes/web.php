<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;


Route::get('/error-test', function () {
    Log::info('calling the error route');
    throw new \Exception('something unexpected broke');
});

Route::get('/maintenance', function () {
    Artisan::call('down');
});

Route::get('/production', function () {
    Artisan::call('up');
});

Route::post('/submit-form', function () {
    //
})->middleware(\Spatie\HttpLogger\Middlewares\HttpLogger::class);


Route::group(['namespace' => 'Pembelian'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/pembelian', 'Pembelian@index')->name('pembelianList');
        Route::post('/pembelian/datatable', 'Pembelian@data_table')->name('pembelianDataTable');
        Route::get('/pembelian/create', 'Pembelian@create')->name('pembelianCreate');
        Route::post('/pembelian/no_faktur', 'Pembelian@no_faktur')->name('pembelianNoFaktur');
        Route::post('/pembelian/insert', 'Pembelian@insert')->name('pembelianInsert');

        Route::get('/pembelian/edit/{id_pembelian}', 'Pembelian@edit')->name('pembelianEdit');
        Route::post('/pembelian/update/{id_pembelian}', 'Pembelian@update')->name('pembelianUpdate');
        Route::get('/pembelian/detail/modal/{id_pembelian}', 'Pembelian@detail_modal')->name('pembelianDetailModal');

    });

});

Route::group(['namespace' => 'Barang'], function () {

    Route::group(['middleware' => 'authLogin'], function () {

        // Barang
        Route::get('/barang', "Barang@index")->name('barangList');
        Route::post('/barang/datatable', 'Barang@data_table')->name('barangDataTable');
        Route::post('/barang', "Barang@insert")->name('barangInsert');
        Route::delete('/barang/{id}', "Barang@delete")->name('barangDelete');
        Route::get('/barang/{id}', "Barang@edit_modal")->name('barangEditModal');
        Route::post('/barang/{id}', "Barang@update")->name('barangUpdate');

        // Stok Barang
        Route::get('/stok-barang/{id_barang}', "StokBarang@list")->name('stokBarangList');
        Route::post('/stok-barang/{id_barang}/datatable', 'StokBarang@data_table')->name('stokBarangDataTable');
        Route::get('/stok-barang/{id_barang}/{id_stok_barang}', "StokBarang@edit_modal")->name('stokBarangEditModal');
        Route::post('/stok-barang/{id_barang}/{id_stok_barang}', "StokBarang@update")->name('stokBarangUpdate');
    });

});

Route::group(['namespace' => 'Penjualan'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/penjualan', 'Penjualan@index')->name('penjualanList');
        Route::post('/penjualan/datatable', 'Penjualan@data_table')->name('penjualanDataTable');
        Route::get('/penjualan/create', 'Penjualan@create')->name('penjualanCreate');
        Route::post('/penjualan/insert', 'Penjualan@insert')->name('penjualanInsert');

        Route::post('/penjualan/no_faktur', 'Penjualan@no_faktur')->name('penjualanNoFaktur');
        Route::post('/penjualan/stok_barang', 'Penjualan@stok_barang')->name('penjualanStokBarang');

        Route::get('/penjualan/edit/{id_penjualan}', 'Penjualan@edit')->name('penjualanEdit');
        Route::post('/penjualan/update/{id_penjualan}', 'Penjualan@update')->name('penjualanUpdate');
        Route::delete('/penjualan/delete/{id_penjualan}', 'Penjualan@delete')->name('penjualanDelete');

        Route::get('/penjualan/detail/modal/{id_penjualan}', 'Penjualan@detail_modal')->name('penjualanDetailModal');
        Route::get('/penjualan/setor-uang/{id_penjualan}', 'Penjualan@midtrans_setor_uang_penjualan')->name('penjualanSetorUang');
    });

});

Route::group(['namespace' => 'PenyesuaianStok'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/penyesuaian-stok-barang', 'PenyesuaianStok@index')->name('penyesuaianStokList');
        Route::post('/penyesuaian-stok-barang/datatable', 'PenyesuaianStok@data_table')->name('penyesuaianStokDataTable');
        Route::get('/penyesuaian-stok-barang/modal/{id_barang}', 'PenyesuaianStok@penyesuaian_modal')->name('penyesuaianStokModal');
        Route::post('/penyesuaian-stok-barang/update', 'PenyesuaianStok@update')->name('penyesuaianStokUpdate');

        Route::get('/penyesuaian-stok-barang/history', 'PenyesuaianStok@index_history')->name('penyesuaianStokHistoryList');
        Route::post('/penyesuaian-stok-barang/history/datatable', 'PenyesuaianStok@data_table_history')->name('penyesuaianStokHistoryDataTable');
    });

});

Route::group(['namespace' => 'KartuStok'], function () {
    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/kartu-stok', 'KartuStok@index')->name('kartuStokList');
        Route::post('/kartu-stok/datatable', 'KartuStok@data_table')->name('kartuStokDataTable');
    });

});

Route::group(['namespace' => 'StokAlert'], function () {
    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/stok-alert', 'StokAlert@index')->name('stokAlertList');
        Route::post('/stok-alert/datatable', 'StokAlert@data_table')->name('stokAlertDataTable');
    });
});

Route::group(['namespace' => 'HutangPiutang'], function () {
    Route::group(['middleware' => 'authLogin'], function () {


        /**
         * Hutang Lain Lain
         */
        Route::get('/hutang-lain', 'HutangLain@index')->name('hutangLainList');
        Route::post('/hutang-lain/datatable', 'HutangLain@data_table')->name('hutangLainDataTable');
        Route::post('/hutang-lain/insert', 'HutangLain@insert')->name('hutangLainInsert');
        Route::delete('/hutang-lain/delete/{id_hutang_lain}', 'HutangLain@delete')->name('hutangLainDelete');

        Route::get('/hutang-lain/pembayaran/modal/{id_hutang_lain}', 'HutangLain@pembayaran_modal')->name('hutangLainPembayaranModal');
        Route::post('/hutang-lain/pembayaran', 'HutangLain@pembayaran_insert')->name('hutangLainPembayaranInsert');
        Route::get('/hutang-lain/pembayaran/modal/history/{id_hutang_lain}', 'HutangLain@pembayaran_history_modal')->name('hutangLainPembayaranHistoryModal');

        Route::get('/hutang-lain/lunas', 'HutangLain@index_lunas')->name('hutangLainLunasList');
        Route::post('/hutang-lain/lunas/datatable', 'HutangLain@data_table_lunas')->name('hutangLainLunasDataTable');

        /**
         * Hutang Supplier
         */
        Route::get('/hutang-supplier', 'HutangSupplier@index')->name('hutangSupplierList');
        Route::post('/hutang-supplier/datatable', 'HutangSupplier@data_table')->name('hutangSupplierDataTable');
        Route::get('/hutang-supplier/pembayaran/modal/{id_hutang_supplier}', 'HutangSupplier@pembayaran_modal')->name('hutangSupplierPembayaranModal');
        Route::post('/hutang-supplier/pembayaran', 'HutangSupplier@pembayaran_insert')->name('hutangSupplierPembayaranInsert');
        Route::get('/hutang-supplier/pembayaran/modal/history/{id_hutang_supplier}', 'HutangSupplier@pembayaran_history_modal')->name('hutangSupplierPembayaranHistoryModal');

        Route::get('/hutang-supplier/lunas', 'HutangSupplier@index_lunas')->name('hutangSupplierLunasList');
        Route::post('/hutang-supplier/lunas/datatable', 'HutangSupplier@data_table_lunas')->name('hutangSupplierLunasDataTable');


        /**
         * Piutang Lain Lain
         */
        Route::get('/piutang-lain', 'PiutangLain@index')->name('piutangLainList');
        Route::post('/piutang-lain/datatable', 'PiutangLain@data_table')->name('piutangLainDataTable');
        Route::post('/piutang-lain/insert', 'PiutangLain@insert')->name('piutangLainInsert');
        Route::delete('/piutang-lain/delete/{id_piutang_lain}', 'PiutangLain@delete')->name('piutangLainDelete');

        Route::get('/piutang-lain/pembayaran/modal/{id_piutang_lain}', 'PiutangLain@pembayaran_modal')->name('piutangLainPembayaranModal');
        Route::post('/piutang-lain/pembayaran', 'PiutangLain@pembayaran_insert')->name('piutangLainPembayaranInsert');
        Route::get('/piutang-lain/pembayaran/modal/history/{id_piutang_lain}', 'PiutangLain@pembayaran_history_modal')->name('piutangLainPembayaranHistoryModal');

        Route::get('/piutang-lain/lunas', 'PiutangLain@index_lunas')->name('piutangLainLunasList');
        Route::post('/piutang-lain/lunas/datatable', 'PiutangLain@data_table_lunas')->name('piutangLainLunasDataTable');

        /**
         * Piutang Pelanggan
         */
        Route::get('/piutang-pelanggan', 'PiutangPelanggan@index')->name('piutangPelangganList');
        Route::post('/piutang-pelanggan/datatable', 'PiutangPelanggan@data_table')->name('piutangPelangganDataTable');
        Route::get('/piutang-pelanggan/pembayaran/modal/{id_piutang_pelanggan}', 'PiutangPelanggan@pembayaran_modal')->name('piutangPelangganPembayaranModal');
        Route::post('/piutang-pelanggan/pembayaran', 'PiutangPelanggan@pembayaran_insert')->name('piutangPelangganPembayaranInsert');
        Route::get('/piutang-pelanggan/pembayaran/modal/history/{id_piutang_pelanggan}', 'PiutangPelanggan@pembayaran_history_modal')->name('piutangPelangganPembayaranHistoryModal');

        Route::get('/piutang-pelanggan/lunas', 'PiutangPelanggan@index_lunas')->name('piutangPelangganLunasList');
        Route::post('/piutang-pelanggan/lunas/datatable', 'PiutangPelanggan@data_table_lunas')->name('piutangPelangganLunasDataTable');

    });
});


Route::group(['namespace' => 'Laporan'], function () {
    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/laporan', 'Laporan@index')->name('laporanList');
        Route::post('/laporan/pembelian/datatable', 'Laporan@data_table_pembelian')->name('laporanPembelianDataTable');
        Route::post('/laporan/penjualan/datatable', 'Laporan@data_table_penjualan')->name('laporanPenjualanDataTable');

        Route::post('/laporan/cetak/pdf', 'Laporan@cetak_pdf')->name('laporanCetakPdf');
    });
});

Route::namespace('General')->group(function () {

    Route::get('undercosntruction', "Underconstruction@index")->name('underconstructionPage');
    Route::post('city/list', "General@city_list")->name('cityList');
    Route::post('subdistrict/list', "General@subdistrict_list")->name('subdistrictList');
    Route::post('ongkir/list', "General@ongkir")->name('ongkirList');

    Route::group(['middleware' => 'checkLogin'], function () {
        Route::get('/', "Login@index")->name("loginPage");
        Route::post('/roles/login', 'Login@roles')->name("rolesLogin");
        Route::post('/login', "Login@do")->name("loginDo");
    });

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/dashboard', "Dashboard@index")->name('dashboardPage');
        Route::get('/whatsapp-test', "Dashboard@whatsapp_test")->name('whatsappTest');
        Route::get('/logout', "Logout@do")->name('logoutDo');
        Route::get('/profile', "Profile@index")->name('profilPage');
        Route::post('/profile/administrator', "Profile@update_administrator")->name('profilAdministratorUpdate');

        Route::post('/centak/pdf', "General@cetak_pdf")->name('cetakPdf');

    });
});

// =============== Master Data =================== //

Route::group(['namespace' => 'MasterData', 'middleware' => 'authLogin'], function () {

    // Satuan
    Route::get('/satuan', "Satuan@index")->name('satuanList');
    Route::post('/satuan', "Satuan@insert")->name('satuanInsert');
    Route::delete('/satuan/{id}', "Satuan@delete")->name('satuanDelete');
    Route::get('/satuan/{id}', "Satuan@edit_modal")->name('satuanEditModal');
    Route::post('/satuan/{id}', "Satuan@update")->name('satuanUpdate');

    // Jenis Barang
    Route::get('/jenis-barang', "JenisBarang@index")->name('jenisBarangList');
    Route::post('/jenis-barang', "JenisBarang@insert")->name('jenisBarangInsert');
    Route::delete('/jenis-barang/{id}', "JenisBarang@delete")->name('jenisBarangDelete');
    Route::get('/jenis-barang/{id}', "JenisBarang@edit_modal")->name('jenisBarangEditModal');
    Route::post('/jenis-barang/{id}', "JenisBarang@update")->name('jenisBarangUpdate');

    // Supplier
    Route::get('/supplier', "Supplier@index")->name('supplierList');
    Route::post('/supplier', "Supplier@insert")->name('supplierInsert');
    Route::delete('/supplier/{id}', "Supplier@delete")->name('supplierDelete');
    Route::get('/supplier/{id}', "Supplier@edit_modal")->name('supplierEditModal');
    Route::post('/supplier/{id}', "Supplier@update")->name('supplierUpdate');

    // Supplier
    Route::get('/pelanggan', "Pelanggan@index")->name('pelangganList');
    Route::post('pelanggan', "Pelanggan@insert")->name('pelangganInsert');
    Route::delete('/pelanggan/{id}', "Pelanggan@delete")->name('pelangganDelete');
    Route::get('/pelanggan/{id}', "Pelanggan@edit_modal")->name('pelangganEditModal');
    Route::post('/pelanggan/{id}', "Pelanggan@update")->name('pelangganUpdate');

    // Karyawan
    Route::get('/karyawan', "Karyawan@index")->name('karyawanPage');
    Route::post('/karyawan', "Karyawan@insert")->name('karyawanInsert');
    Route::get('/karyawan-list', "Karyawan@list")->name('karyawanList');
    Route::delete('/karyawan/{id}', "Karyawan@delete")->name('karyawanDelete');
    Route::get('/karyawan/{id}', "Karyawan@edit_modal")->name('karyawanEditModal');
    Route::post('/karyawan/{id}', "Karyawan@update")->name('karyawanUpdate');

    // User
    Route::get('/user', "User@index")->name('userPage');
    Route::post('/user', "User@insert")->name('userInsert');
    Route::get('/user-list', "User@list")->name('userList');
    Route::delete('/user/{kode}', "User@delete")->name('userDelete');
    Route::get('/user/modal/{kode}', "User@edit_modal")->name('userEditModal');
    Route::post('/user/{kode}', "User@update")->name('userUpdate');

    // Role User
    Route::get('/user-role', "UserRole@index")->name('userRolePage');
    Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
    Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
    Route::get('/user-role/modal/{id}', "UserRole@edit")->name('userRoleEditModal');
    Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
    Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
    Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');

    // Metode Tindakan
    Route::get('/metode-tindakan', "ActionMethod@index")->name('actionMethodPage');
    Route::post('/metode-tindakan', "ActionMethod@insert")->name('actionMethodInsert');
    Route::get('/metode-tindakan-list', "ActionMethod@list")->name('actionMethodList');
    Route::delete('/metode-tindakan/{kode}', "ActionMethod@delete")->name('actionMethodDelete');
    Route::get('/metode-tindakan/modal/{kode}', "ActionMethod@edit_modal")->name('actionMethodEditModal');
    Route::post('/metode-tindakan/{kode}', "ActionMethod@update")->name('actionMethodUpdate');

});

Route::group(['prefix' => 'midtrans', 'namespace' => 'Midtrans'], function() {

    /**
     * Notification
     */

    Route::post('/notif/payment', 'Notif@payment')->name('notifPayment');
    Route::post('/notif/recurring', 'Notif@recurring')->name('notifRecurring');
    Route::post('/notif/pay-account', 'Notif@pay_account')->name('notifPayAccount');
    Route::get('/finish', 'Notif@finish')->name('midtransFinish');
    Route::get('/unfinish', 'Notif@unfinish')->name('midtransUnfinish');
    Route::get('/error', 'Notif@error')->name('midtransError');

    /**
     * Direct Pay
     */
    Route::get('/vtweb', 'VtwebController@vtweb');

    /**
     * Bayar pakai kartu kredit
     */
    Route::get('/vtdirect', 'VtdirectController@vtdirect');
    Route::post('/vtdirect', 'VtdirectController@checkout_process')->name('vtdirectInsert');


    Route::get('/vt_transaction', 'TransactionController@transaction');
    Route::post('/vt_transaction', 'TransactionController@transaction_process');

    Route::post('/vt_notif', 'VtwebController@notification');

    Route::get('/snap', 'SnapController@snap');
    Route::get('/snaptoken', 'SnapController@token');
    Route::post('/snapfinish', 'SnapController@finish');
});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    //Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

