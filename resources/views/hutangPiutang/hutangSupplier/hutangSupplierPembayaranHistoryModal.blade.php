<div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xlg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">History Pembayaran Hutang Supplier</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body">

                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Supplier</label>
                        <div class="col-9 col-form-label">
                            {{ $hutang_supplier->supplier->spl_kode.' '.$hutang_supplier->supplier->spl_nama }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Faktur Pembelian</label>
                        <div class="col-9 col-form-label">
                            {{ $hutang_supplier->pembelian->pbl_no_faktur }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Tanggal Hutang</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_date_label($hutang_supplier->hsp_tanggal) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Jatuh Tempo</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_date_label($hutang_supplier->hsp_tanggal_jatuh_tempo) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Total Hutang</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_number($hutang_supplier->hsp_total) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Sisa Hutang</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_number($hutang_supplier->hsp_sisa) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Keterangan Hutang</label>
                        <div class="col-9 col-form-label">
                            {{ $hutang_supplier->hsp_keterangan }}
                        </div>
                    </div>

                    <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Tanggal Bayar</th>
                            <th>Total Hutang</th>
                            <th>Jumlah Bayar</th>
                            <th>Sisa Bayar</th>
                            <th>Keterangan</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $key => $row)
                                <tr>
                                    <td>{{ ++$key }}.</td>
                                    <td>{{ Main::format_date_label($row->hsp_tanggal_bayar) }}</td>
                                    <td>{{ Main::format_number($row->hsp_total_hutang) }}</td>
                                    <td>{{ Main::format_number($row->hsp_jumlah_bayar) }}</td>
                                    <td>{{ Main::format_number($row->hsp_sisa_bayar) }}</td>
                                    <td>{{ $row->hsp_keterangan }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>