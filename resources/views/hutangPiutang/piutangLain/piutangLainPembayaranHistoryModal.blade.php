<div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xlg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">History Pembayaran Piutang Lain Lain</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Tanggal Piutang</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_date_label($piutang_lain->ptl_tanggal) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Jatuh Tempo</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_date_label($piutang_lain->ptl_tanggal_jatuh_tempo) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Total Piutang</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_number($piutang_lain->ptl_total) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Sisa Piutang</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_number($piutang_lain->ptl_sisa) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Keterangan Piutang</label>
                        <div class="col-9 col-form-label">
                            {{ $piutang_lain->ptl_keterangan }}
                        </div>
                    </div>

                    <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Tanggal Bayar</th>
                            <th>Total Piutang</th>
                            <th>Jumlah Bayar</th>
                            <th>Sisa Bayar</th>
                            <th>Keterangan</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $key => $row)
                                <tr>
                                    <td>{{ ++$key }}.</td>
                                    <td>{{ Main::format_date_label($row->plp_tanggal_bayar) }}</td>
                                    <td>{{ Main::format_number($row->plp_total_piutang) }}</td>
                                    <td>{{ Main::format_number($row->plp_jumlah_bayar) }}</td>
                                    <td>{{ Main::format_number($row->plp_sisa_bayar) }}</td>
                                    <td>{{ $row->plp_keterangan }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>