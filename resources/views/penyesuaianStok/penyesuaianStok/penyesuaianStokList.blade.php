@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            {!! $date_filter !!}

            <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 0">
                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('penyesuaianStokList') }}">
                        <i class="flaticon-list"></i>
                        Daftar Barang
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('penyesuaianStokHistoryList') }}">
                        <i class="flaticon-time"></i>
                        History Penyesuaian Stok
                    </a>
                </li>
            </ul>
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-bordered datatable-new"
                           data-url="{{ route('penyesuaianStokDataTable') }}"
                           data-column="{{ json_encode($datatable_column) }}">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Golongan</th>
                            <th>Minimal Stok</th>
                            <th>Stok Sekarang</th>
                            <th width="100">Menu</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
