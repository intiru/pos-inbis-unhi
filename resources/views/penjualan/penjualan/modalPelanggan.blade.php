<div class="modal" id="modal-pelanggan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-success">
                <h5 class="modal-title m--font-light" id="exampleModalLabel">Pilih Pelanggan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_2">
                    <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Kode Pelanggan</th>
                        <th>Nama Pelanggan</th>
                        <th>Alamat Pelanggan</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php($no = 1)
                        @foreach($pelanggan as $r)
                            <tr data-id-pelanggan="{{ $r->id_pelanggan }}"
                                data-plg-kode="{{ $r->plg_kode }}"
                                data-plg-nama="{{ $r->plg_nama }}"
                                data-plg-alamat="{{ $r->plg_alamat }}"
                                data-plg-phone="{{ $r->plg_telepon }}">
                                <td width="10">{{ $no++ }}.</td>
                                <td>{{ $r->plg_kode }}</td>
                                <td>{{ $r->plg_nama }}</td>
                                <td>{{ $r->plg_alamat }}</td>
                                <td width="30">
                                    <button type="button" class="btn-pelanggan-select btn btn-success btn-sm m-btn--pill">
                                        <i class="la la-check"></i> Pilih
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>