<div class="modal" id="modal-bahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-accent">
                <h5 class="modal-title m--font-light" id="exampleModalLabel">Pilih Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped- table-bordered table-hover table-checkable datatable-new-2">
                    <thead>
                    <tr>
                        <th width="20">No</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Total Stok</th>
                        <th width="40">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php($no = 1)
                        @foreach($barang as $r)
                            <tr data-id-barang="{{ $r->id_barang }}"
                                data-kode-barang="{{ $r->brg_kode }}"
                                data-nama-barang="{{ $r->brg_nama }}">
                                <td>{{ $no++ }}.</td>
                                <td>{{ $r->brg_kode }}</td>
                                <td>{{ $r->brg_nama }}</td>
                                <td>{{ $r->total_stok }}</td>
                                <td>
                                    <div class="btn-group m-btn-group m-btn--pill">
                                        <button type="button"
                                                class="btn-bahan-select btn btn-success btn-sm m-btn--pill">
                                            <i class="la la-check"></i> Pilih
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>