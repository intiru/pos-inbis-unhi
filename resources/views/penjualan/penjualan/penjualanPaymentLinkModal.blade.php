<div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Link Payment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body detail-info">
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-12 col-form-label">Link Payment Gateway</label>
                        <div class="col-12 col-form-label">
                            <textarea class="form-control" id="midtrans-payment-link">{{ $pjl_midtrans_payment_link }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="myFunction()">Copy Link</button>
                <a href="{{ route('penjualanList') }}" class="btn btn-info">Ke Daftar Penjualan</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function myFunction() {
        var copyText = document.getElementById("midtrans-payment-link");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        swal({
            title: "Berhasil di Copy",
            html: "Link : "+copyText.value,
            type: 'success',
            showDenyButton: true,
            confirmButtonText: 'Okay',
        }).then(function() {
            window.location.href = $('#base-value').data('route-penjualan-list');
        })
    }
</script>