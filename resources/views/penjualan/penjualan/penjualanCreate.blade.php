@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">
        .input-group.bootstrap-touchspin > .input-group-btn {
            display: none !important;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/penjualan.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('penjualan/penjualan/modalBarang')
    @include('penjualan/penjualan/modalBarangStok')
    @include('penjualan/penjualan/modalPelanggan')
    @include('penjualan/penjualan/trBarang')

    <form action="{{ route('penjualanInsert') }}"
          method="post"
          style="width: 100%"
          data-redirect="{{ route('penjualanList') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          data-confirm-status="true"
          data-confirm-message="Data akan keluar dari stok barang, jika terjadi kesalahan input jumlah barang, silahkan perbaiki di <strong>Menu Penyesuaian Stok</strong>"
          class="form-send">

        {{ csrf_field() }}

        <input type="hidden" name="urutan" value="">
        <input type="hidden" name="no_faktur">
        <input type="hidden" name="id_pelanggan" value="{{ $pelanggan_first->id_pelanggan }}">
        <input type="hidden" name="plg_kode" value="{{ $pelanggan_first->plg_kode }}">
        <input type="hidden" name="total" value="0">
        <input type="hidden" name="grand_total" value="0">
        <input type="hidden" name="sisa_pembayaran" value="0">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Faktur Penjualan</label>
                                <div class="col-9 col-form-label no_faktur">-</div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Penjualan</label>
                                <div class="col-6">

                                    <input type="text"
                                           class="form-control m_datetimepicker"
                                           readonly=""
                                           name="tanggal"
                                           placeholder="Select date &amp; time"
                                           value="{{ date('d-m-Y H:i') }}">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Nama Pembeli</label>
                                <div class="col-6">

                                    <input type="text"
                                           class="form-control"
                                           name="pbl_nama">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Phone/WhatsApp</label>
                                <div class="col-6">

                                    <input type="text"
                                           class="form-control"
                                           name="pbl_phone">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Pilih Kurir Pengiriman</label>
                                <div class="col-6">
                                    <select name="pjl_jenis_kurir" class="form-control">
                                        <option value="jne">JNE</option>
                                        <option value="jnt">J&T</option>
                                        <option value="pos">POS</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group m-form__group row m--hide">
                                <label for="example-text-input" class="col-3 col-form-label">Pelanggan</label>
                                <div class="col-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control nama_pelanggan"
                                               placeholder="{{ '( '.$pelanggan_first->plg_kode.') '.$pelanggan_first->plg_nama }}"
                                               disabled>
                                        <div class="input-group-append">
                                            <button class="btn btn-success m-btn--pill"
                                                    type="button"
                                                    data-toggle="modal"
                                                    data-target="#modal-pelanggan">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row m--hide">
                                <label for="example-text-input" class="col-3 col-form-label">Nomer Telepon</label>
                                <div class="col-2">
                                    <div class="input-group col-form-label telp_supplier">-</div>
                                </div>
                                <label for="example-text-input" class="col-2 col-form-label">Alamat</label>
                                <div class="col-4">
                                    <div class="input-group col-form-label alamat_supplier">-</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jenis Pembayaran</label>
                                <div class="col-6">
                                    <select name="pembayaran" class="form-control m-select2">
{{--                                        <option value="cash">Cash</option>--}}
                                        <option value="payment_gateway">Payment Gateway</option>
{{--                                        <option value="kredit">Kredit</option>
                                        <option value="transfer">Transfer</option>--}}
                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Provinsi Pembeli</label>
                                <div class="col-6">
                                    <select name="id_province" class="form-control m-select2">
                                        @foreach($province as $row)
                                            <option value="{{ $row->id_province }}">{{ $row->province_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Kabupaten Pembeli</label>
                                <div class="col-6">
                                    <select name="id_city" class="form-control m-select2">
                                        @foreach($city as $row)
                                            <option value="{{ $row->id_city }}" {{ $row->id_city == '114' ? 'selected':'' }}>{{ $row->city_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Kecamatan Pembeli</label>
                                <div class="col-6">
                                    <select name="id_subdistrict" class="form-control m-select2">
                                        @foreach($subdistrict as $row)
                                            <option value="{{ $row->id_subdistrict }}" {{ $row->id_subdistrict == '114' ? 'selected':'' }}>{{ $row->subdistrict_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Alamat/Jalan Rumah</label>
                                <div class="col-6">
                                    <textarea class="form-control" name="pbl_alamat_tinggal" placeholder="Jln. Melati No. 1"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-bahan-row-add btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Barang
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-bahan table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th align="center">Nama Barang</th>
                                <th>Stok Barang</th>
                                <th>Harga Jual</th>
                                <th class="m--hide">PPN({{ $ppnPersen }}%)</th>
                                <th class="m--hide">Harga Net</th>
                                <th>Stok</th>
                                <th>Qty</th>
                                <th>Subtotal</th>
                                <th width="100">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-lg-5 offset-lg-7">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                        <div class="col-8 col-form-label total">0</div>
                                    </div>
                                {{--    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label m--hide">Biaya
                                            Tambahan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="biaya_tambahan" value="0">
                                        </div>
                                    </div>--}}
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label ">Ongkos Kirim</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral m--hide" type="text"
                                                   name="biaya_tambahan" value="0">
                                            <select name="pjl_ongkos_kirim" class="form-control">
{{--                                                @foreach($ongkir_list as $row)--}}
{{--                                                    <option value="{{ $row['cost'][0]['value'] }}">{{ $row['description'].' - '.Main::format_number($row['cost'][0]['value']). ' - '.$row['cost'][0]['etd'].' hari' }}</option>--}}
{{--                                                @endforeach--}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="potongan" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                        <div class="col-8 col-form-label grand-total"
                                             style="font-size: 20px; font-weight: bold">0
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group m-form__group row div-jumlah-bayar m--hide">
                                        <label for="example-text-input" class="col-4 col-form-label">Jumlah
                                            Bayar</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="jumlah_bayar" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row div-sisa-piutang m--hide">
                                        <label for="example-text-input" class="col-4 col-form-label">Sisa
                                            (Piutang)</label>
                                        <div class="col-8 col-form-label sisa-pembayaran"
                                             style="font-size: 20px; font-weight: bold">0
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row div-jatuh-tempo m--hide">
                                        <label for="example-text-input" class="col-4 col-form-label">Tanggal Jatuh
                                            Tempo</label>
                                        <div class="col-8">
                                            <input type="text"
                                                   name="tanggal_jatuh_tempo"
                                                   class="form-control m-input m_datepicker_top"
                                                   readonly=""
                                                   value="{{ date('d-m-Y') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="produksi-buttons">
            <button type="submit"
                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan Penjualan</span>
                        </span>
            </button>

            <a href="{{ route('penjualanList') }}"
               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
            </a>
        </div>

    </form>
@endsection