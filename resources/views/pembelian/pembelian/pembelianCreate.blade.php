@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">
        .input-group.bootstrap-touchspin > .input-group-btn {
            display: none !important;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/pembelian.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('pembelian/pembelian/modalBarang')
    {{--    @include('pembelian/pembelian/modalBahanStok')--}}
    @include('pembelian/pembelian/modalSupplier')
    @include('pembelian/pembelian/trBarang')

    <form action="{{ route('pembelianInsert') }}"
          method="post"
          style="width: 100%"
          data-redirect="{{ route('pembelianList') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          data-confirm-status="true"
          data-confirm-message="Data akan masuk ke stok barang, jika terjadi kesalahan input jumlah barang, silahkan perbaiki di <strong>Menu Penyesuaian Stok</strong>"
          class="form-send">

        {{ csrf_field() }}

        <input type="hidden" name="urutan" value="">
        <input type="hidden" name="no_faktur">
        <input type="hidden" name="id_supplier">
        <input type="hidden" name="kode_supplier">
        <input type="hidden" name="total" value="0">
        <input type="hidden" name="grand_total" value="0">
        <input type="hidden" name="sisa_pembayaran" value="0">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Pembelian</label>
                                <div class="col-6">
                                    <input type="text"
                                           class="form-control m_datetimepicker"
                                           readonly=""
                                           name="tanggal"
                                           placeholder="Select date &amp; time"
                                            value="{{ date('d-m-Y H:i') }}">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Supplier</label>
                                <div class="col-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control nama_supplier"
                                               placeholder="Nama Supplier..."
                                               disabled>
                                        <div class="input-group-append">
                                            <button class="btn btn-success m-btn--pill"
                                                    type="button"
                                                    data-toggle="modal"
                                                    data-target="#modal-supplier">
                                                <i class="la la-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Nomer Telepon</label>
                                <div class="col-2">
                                    <div class="input-group col-form-label telp_supplier">-</div>
                                </div>
                                <label for="example-text-input" class="col-2 col-form-label">Alamat</label>
                                <div class="col-4">
                                    <div class="input-group col-form-label alamat_supplier">-</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Faktur Pembelian</label>
                                <div class="col-9 col-form-label no_faktur">-</div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Pembayaran</label>
                                <div class="col-6">
                                    <select name="pembayaran" class="form-control m-select2">
                                        <option value="cash">Cash</option>
                                        <option value="kredit">Kredit</option>
                                        <option value="transfer">Transfer</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan/Catatan</label>
                                <div class="col-9">
                                    <textarea class="form-control" name="keterangan"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-bahan-row-add btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Barang
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-bahan table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Kode Batch</th>
                                <th>Expired Date</th>
                                <th>Satuan</th>
                                <th>Konsinyasi ?</th>
                                <th>Harga Beli</th>
                                <th>Harga Jual</th>
                                <th>PPN({{ $ppnPersen }}%)</th>
                                <th>Harga Net</th>
                                <th>Qty</th>
                                <th>Subtotal</th>
                                <th width="100">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-lg-5 offset-lg-7">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                        <div class="col-8 col-form-label total">0</div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Biaya
                                            Tambahan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="biaya_tambahan" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="potongan" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                        <div class="col-8 col-form-label grand-total"
                                             style="font-size: 20px; font-weight: bold">0
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Jumlah
                                            Bayar</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="jumlah_bayar" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Sisa
                                            (Hutang)</label>
                                        <div class="col-8 col-form-label sisa-pembayaran"
                                             style="font-size: 20px; font-weight: bold">0
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row div-jatuh-tempo m--hide">
                                        <label for="example-text-input" class="col-4 col-form-label">Tanggal Jatuh
                                            Tempo</label>
                                        <div class="col-8">
                                            <input type="text"
                                                   name="tanggal_jatuh_tempo"
                                                   class="form-control m-input m_datepicker_top"
                                                   readonly=""
                                                   value="{{ date('d-m-Y') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="produksi-buttons">
            <button type="submit"
                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan Pembelian</span>
                        </span>
            </button>

            <a href="{{ route('pembelianList') }}"
               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
            </a>
        </div>

    </form>
@endsection