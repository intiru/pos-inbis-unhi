@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">
        .input-group.bootstrap-touchspin > .input-group-btn {
            display: none !important;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/pembelian.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    <form action="{{ route('pembelianUpdate', ['id_pembelian'=>Main::encrypt($edit->id_pembelian)]) }}"
          method="post"
          style="width: 100%"
          data-redirect="{{ route('pembelianList') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          data-confirm-status="false"
          data-confirm-message="Data akan masuk ke stok barang, jika terjadi kesalahan input jumlah barang, silahkan perbaiki di <strong>Menu Penyesuaian Stok</strong>"
          class="form-send">

        {{ csrf_field() }}

        <input type="hidden" name="urutan" value="">
        <input type="hidden" name="no_faktur" value="{{ $edit->no_faktur }}">
        <input type="hidden" name="id_supplier" value="{{ $edit->id_supplier }}">
        <input type="hidden" name="kode_supplier" value="{{ $edit->supplier->spl_kode }}">
        <input type="hidden" name="total" value="{{ $edit->pbl_total }}">
        <input type="hidden" name="grand_total" value="{{ $edit->pbl_grand_total }}">
        <input type="hidden" name="sisa_pembayaran" value="{{ $edit->pbl_sisa_pembayaran }}">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Pembelian</label>
                                <div class="col-6">
                                    <input type="text"
                                           class="form-control m_datetimepicker"
                                           readonly=""
                                           name="tanggal"
                                           placeholder="Select date &amp; time"
                                           value="{{ date('d-m-Y H:i', strtotime($edit->pbl_tanggal_order)) }}">
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Supplier</label>
                                <div class="col-8 col-form-label">
                                    {{ "(".$edit->supplier->spl_kode.") ".$edit->supplier->spl_nama }}
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Nomer Telepon</label>
                                <div class="col-2">
                                    <div class="input-group col-form-label telp_supplier">{{ $edit->supplier->spl_phone }}</div>
                                </div>
                                <label for="example-text-input" class="col-2 col-form-label">Alamat</label>
                                <div class="col-4">
                                    <div class="input-group col-form-label alamat_supplier">{{ $edit->supplier->spl_alamat }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Faktur Pembelian</label>
                                <div class="col-9 col-form-label no_faktur">{{ $edit->pbl_no_faktur }}</div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Pembayaran</label>
                                <div class="col-6">
                                    <select name="pembayaran" class="form-control m-select2">
                                        <option value="cash" {{ $edit->pembayaran == 'cash' ? "selected":"" }}>Cash
                                        </option>
                                        <option value="kredit" {{ $edit->pembayaran == 'kredit' ? "selected":"" }}>
                                            Kredit
                                        </option>
                                        <option value="transfer" {{ $edit->pembayaran == 'transfer' ? "selected":"" }}>
                                            Transfer
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan/Catatan</label>
                                <div class="col-9">
                                    <textarea class="form-control" name="keterangan">{{ $edit->pbl_keterangan }}</textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <table class="table-bahan table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Kode Batch</th>
                                <th>Expired Date</th>
                                <th>Satuan</th>
                                <th>Konsinyasi ?</th>
                                <th>Harga Beli</th>
                                <th>Harga Jual</th>
                                <th>PPN({{ $ppnPersen }}%)</th>
                                <th>Harga Net</th>
                                <th>Qty</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($edit->pembelian_detail as $key => $row)
                                <tr data-index="{{ ++$key }}">
                                    <td class="m--hide">
                                        <input type="hidden" name="id_pembelian_detail[]" value="{{ $row->id_pembelian_detail }}">
                                        <input type="hidden" name="id_barang[]" value="{{ $row->id_barang }}">
                                        <input type="hidden" name="harga_net[]" value="{{ $row->pbd_harga_net }}">
                                        <input type="hidden" name="sub_total[]" value="{{ $row->pbd_sub_total }}">
                                        <input type="hidden" name="ppn_nominal[]" value="{{ $row->pbd_ppn_nominal }}">
                                        <input type="hidden" name="qty[]" value="{{ $row->pbd_qty }}">
                                    </td>
                                    <td>{{ $row->barang->brg_kode.' '.$row->barang->brg_nama }}</td>
                                    <td>
                                        <input type="text" name="kode_batch[]" class="form-control m-input" value="{{ $row->pbd_kode_batch }}">
                                    </td>
                                    <td>
                                        <input type="text" name="expired_date[]"
                                               class="form-control m-input m_datepicker"
                                               value="{{ date('d-m-Y', strtotime($row->pbd_expired)) }}">
                                    </td>
                                    <td>
                                        <select class="form-control m-input" name="id_satuan[]">
                                            @foreach($satuan as $row_satuan)
                                                <option value="{{ $row_satuan->id_satuan }}" {{ $row->id_satuan == $edit->id_satuan ? 'selected':'' }}>{{ $row_satuan->stn_nama }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control" name="konsinyasi[]">
                                            <option value="yes" {{ $edit->pbd_konsinyasi_status == 'yes' ? 'selected':'' }}>Ya</option>
                                            <option value="no" {{ $edit->pbd_konsinyasi_status == 'no' ? 'selected':'' }}>Tidak</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="harga_beli[]"
                                               class="form-control m-input input-numeral" value="{{ Main::format_number($row->pbd_harga_beli) }}">
                                    </td>
                                    <td>
                                        <input type="text" name="harga_jual[]"
                                               class="form-control m-input input-numeral" value="{{ Main::format_number($row->pbd_harga_jual) }}">
                                    </td>
                                    <td class="ppn-nominal">{{ Main::format_number($row->pbd_ppn_nominal) }}</td>
                                    <td class="harga-net">{{ Main::format_number($row->pbd_harga_net) }}</td>
                                    <td>{{ Main::format_number($row->pbd_qty) }}</td>
                                    <td class="sub-total">{{ Main::format_number($row->pbd_sub_total) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-lg-5 offset-lg-7">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                        <div class="col-8 col-form-label total">{{ Main::format_number($edit->pbl_total) }}</div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Biaya
                                            Tambahan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="biaya_tambahan" value="{{ $edit->pbl_biaya_tambahan }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="potongan" value="{{ $edit->pbl_potongan }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                        <div class="col-8 col-form-label grand-total"
                                             style="font-size: 20px; font-weight: bold">{{ Main::format_number($edit->pbl_grand_total) }}
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Jumlah
                                            Bayar</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="jumlah_bayar" value="{{ $edit->pbl_jumlah_bayar }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Sisa
                                            (Hutang)</label>
                                        <div class="col-8 col-form-label sisa-pembayaran"
                                             style="font-size: 20px; font-weight: bold">{{ Main::format_number($edit->pbl_sisa_pembayaran) }}
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row div-jatuh-tempo {{ $edit->pbl_sisa_pembayaran > 0 ? '':'m--hide' }}">
                                        <label for="example-text-input" class="col-4 col-form-label">Tanggal Jatuh
                                            Tempo</label>
                                        <div class="col-8">
                                            <input type="text"
                                                   name="tanggal_jatuh_tempo"
                                                   class="form-control m-input m_datepicker_top"
                                                   readonly=""
                                                   value="{{ date('d-m-Y', strtotime($edit->pbl_jatuh_tempo)) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="produksi-buttons">
            <button type="submit"
                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan Pembelian</span>
                        </span>
            </button>

            <a href="{{ route('pembelianList') }}"
               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
            </a>
        </div>

    </form>
@endsection