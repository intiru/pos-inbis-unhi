<div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xxlg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Pembelian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body detail-info">
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Supplier</label>
                        <div class="col-9 col-form-label">
                            {{ $pembelian->supplier->spl_kode.' '.$pembelian->supplier->spl_nama }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">User Aplikasi</label>
                        <div class="col-9 col-form-label">
                            {{ $pembelian->user->karyawan->nama_karyawan }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Faktur Pembelian</label>
                        <div class="col-9 col-form-label">
                            {{ $pembelian->pbl_no_faktur }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Tanggal Pembelian</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_date($pembelian->pbl_tanggal_order) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Jenis Pembayaran</label>
                        <div class="col-9 col-form-label">
                            {{ $pembelian->pbl_jenis_pembayaran }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
                        <div class="col-9 col-form-label">
                            {{ $pembelian->pbl_keterangan }}
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Nama Barang</th>
                                <th>Satuan</th>
                                <th>Kode Batch</th>
                                <th>Harga Beli</th>
                                <th>Harga Jual</th>
                                <th>Expired</th>
                                <th>PPn (%)</th>
                                <th>PPn</th>
                                <th>Harga Net</th>
                                <th>Qty</th>
                                <th>Sub Total</th>
                                <th>Konsinyasi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pembelian->pembelian_detail as $key => $row)
                                <tr>
                                    <td>{{ ++$key }}.</td>
                                    <td>{{ $row->barang->brg_kode.' '.$row->barang->brg_nama }}</td>
                                    <td>{{ $row->satuan->stn_nama }}</td>
                                    <td>{{ $row->pbd_kode_batch }}</td>
                                    <td>{{ Main::format_money($row->pbd_harga_beli) }}</td>
                                    <td>{{ Main::format_money($row->pbd_harga_jual) }}</td>
                                    <td>{{ Main::format_date_label($row->pbd_expired) }}</td>
                                    <td>{{ $row->pbd_ppn_persen }}</td>
                                    <td>{{ Main::format_money($row->pbd_ppn_nominal) }}</td>
                                    <td>{{ Main::format_money($row->pbd_harga_net) }}</td>
                                    <td>{{ Main::format_number($row->pbd_qty) }}</td>
                                    <td>{{ Main::format_money($row->pbd_sub_total) }}</td>
                                    <td>{!! Main::status($row->pbd_konsinyasi_status)  !!} </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br/>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Total</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_money($pembelian->pbl_tota) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Biaya Tambahan</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_money($pembelian->pbl_biaya_tambahan) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Potongan</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_money($pembelian->pbl_potongan) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Grand Total</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_money($pembelian->pbl_grand_total) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Jumlah Bayar</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_money($pembelian->pbl_jumlah_bayar) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Sisa (Hutang)</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_money($pembelian->pbl_sisa_pembayaran) }}
                        </div>
                    </div>
                    @if($pembelian->pbl_sisa_pembayaran > 0)
                        <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-3 col-form-label">Jatuh Tempo</label>
                            <div class="col-9 col-form-label">
                                {{ Main::format_date_label($pembelian->pbl_jatuh_tempo) }}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>