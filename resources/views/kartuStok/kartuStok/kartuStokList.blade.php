@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            {!! $date_filter !!}

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <table class="table table-bordered datatable-new"
                           data-url="{{ route('kartuStokDataTable') }}"
                           data-column="{{ json_encode($datatable_column) }}"
                           data-data="{{ json_encode($table_data_post) }}">
                        <thead>
                        <tr>
                            <th width="20" rowspan="2" class="text-center">No</th>
                            <th rowspan="2" class="text-center">Tanggal</th>
                            <th rowspan="2" class="text-center">Nama Barang</th>
                            <th rowspan="2" class="text-center">No. Batch</th>
                            <th rowspan="2" class="text-center">Expired Date</th>
                            <th colspan="3" class="text-center">Stok</th>
                            <th rowspan="2" class="text-center">Sisa Barang</th>
                            <th rowspan="2" class="text-center">Sisa Total Barang</th>
                            <th rowspan="2" class="text-center">Keterangan</th>
                        </tr>
                        <tr>
                            <th class="text-center">Awal</th>
                            <th class="text-center">Masuk</th>
                            <th class="text-center">Keluar</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
