<form action="{{ route('userInsert') }}" method="post" class="m-form form-send" autocomplete="off">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Karyawan </label>
                            <select name="id_karyawan" class="form-control m-select2" style="width: 100%">
                                <option value="">Pilih</option>
                                @foreach($karyawan as $r)
                                    <option value="{{ $r->id }}">{{ $r->nama_karyawan }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">User Role </label>
                            <select name="id_user_role" class="form-control m-select2" style="width: 100%">
                                <option value="">Pilih</option>
                                @foreach($user_role as $r)
                                    <option value="{{ $r->id }}">{{ $r->role_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Username </label>
                            <input type="text" class="form-control m-input" name="username" autocomplete="new-username"
                                   autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Password </label>
                            <input type="password" class="form-control m-input" name="password"
                                   autocomplete="new-password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-simpan btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>