<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        body {
            font-size: 11px;
        }

        h1 {
            font-size: 18px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 14px;
            font-weight: bold;
        }

        h4 {
            font-size: 12px;
            font-weight: bold;
        }

        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }

        .bold {
            font-weight: bold;
        }


    </style>
    <?php
    $width_label = '64';
    ?>

</head>
<body>

    <h3 align="center">Laporan Penjualan {{ env('BUSINESS_NAME') }}</h3>
    <center>
        Tanggal : {{ $date_from.' sampai '.$date_to }}
    </center>
    <hr />
    <br />
    <h4 align="center">Rangkuman</h4>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Profit</th>
            <th>Pembelian</th>
            <th>Penjualan</th>
            <th>Hutang</th>
            <th>Piutang</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <th>{{ Main::format_money($profit) }}</th>
                <th>{{ Main::format_money($pembelian) }}</th>
                <th>{{ Main::format_money($penjualan) }}</th>
                <th>{{ Main::format_money($hutang) }}</th>
                <th>{{ Main::format_money($piutang) }}</th>
            </tr>
        </tbody>
    </table>
    <br />
    <h4 align="center">Data Pembelian</h4>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th width="20">No</th>
                <th>Tanggal</th>
                <th>Nama Barang</th>
                <th>Kode Batch</th>
                <th>Harga Beli</th>
                <th>Qty</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data_pembelian as $key => $row)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td class="number">{{ Main::format_datetime($row->pbl_tanggal_order) }}</td>
                    <td>{{ $row->brg_kode . ' ' . $row->brg_nama }}</td>
                    <td>{{ $row->pbd_kode_batch }}</td>
                    <td class="number">{{ Main::format_money($row->pbd_harga_beli) }}</td>
                    <td class="number">{{ Main::format_number($row->pbd_qty) }}</td>
                    <td class="number">{{ Main::format_money($row->pbd_sub_total) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br />
    <h4 align="center">Data Penjualan</h4>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th width="20">No</th>
                <th>Tanggal</th>
                <th>Nama Barang</th>
                <th>Kode Batch</th>
                <th>Harga Jual</th>
                <th>Qty</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data_penjualan as $key => $row)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td class="number">{{ Main::format_datetime($row->pjl_tanggal_penjualan) }}</td>
                    <td>{{ $row->brg_kode . ' ' . $row->brg_nama }}</td>
                    <td>{{ $row->sbr_kode_batch }}</td>
                    <td class="number">{{ Main::format_money($row->pjd_harga) }}</td>
                    <td class="number">{{ Main::format_number($row->pjd_qty) }}</td>
                    <td class="number">{{ Main::format_money($row->pjd_sub_total) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

