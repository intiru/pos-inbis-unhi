<form action="{{ route('barangInsert') }}" method="post" class="m-form form-send" autocomplete="off">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Jenis Barang</label>
                            <select class="form-control m-select2" name="id_jenis_barang" data-placeholder="Pilih Jenis Barang" style="width: 100%">
                                @foreach($jenis_barang as $row)
                                    <option value="{{ $row->id_jenis_barang }}">{{ $row->jbr_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Kode Barang</label>
                            <input type="text" class="form-control m-input" name="brg_kode">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Barang</label>
                            <input type="text" class="form-control m-input" name="brg_nama">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Golongan</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="brg_golongan" value="luar" checked>
                                    Obat Luar
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="brg_golongan" value="dalam">
                                    Obat Dalam
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Minimal Stok</label>
                            <input type="text" class="form-control m-input" name="brg_minimal_stok">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Keterangan</label>
                            <textarea class="form-control" name="brg_keterangan"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-simpan btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>