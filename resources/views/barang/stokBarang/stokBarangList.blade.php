@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                <strong>
                    Perhatian ... !
                </strong>
                <br />
                <ol>
                    <li>Untuk mengedit Qty Stok barang, harap dilakukan pada menu <a href="{{ route('penyesuaianStokList') }}">Penyesuaian Stok</a></li>
                    <li>Untuk mengedit Harga Beli barang, harap dilakukan pada menu <a href="{{ route('pembelianList') }}">Pembelian</a></li>
                </ol>
            </div>
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">



                    <table class="table table-bordered datatable-new"
                           data-url="{{ route('stokBarangDataTable', ['id_barang'=>\app\Helpers\Main::encrypt($id_barang)]) }}"
                           data-column="{{ json_encode($datatable_column) }}">
                        <thead>
                        <tr>
                            <th width="20">No</th>
                            <th>Supplier</th>
                            <th>Kode Batch</th>
                            <th>Expired</th>
                            <th>Qty</th>
                            <th>Harga Beli</th>
                            <th>Harga Jual</th>
                            <th>Konsinyasi</th>
                            <th width="100">Menu</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
