<form action="{{ route('stokBarangUpdate', ['id_barang' => Main::encrypt($id_barang), 'id_stok_barang'=>Main::encrypt($edit->id_stok_barang)]) }}" method="post"
      class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Satuan</label>
                            <select class="form-control m-input" name="id_satuan">
                                @foreach($satuan as $row)
                                    <option value="{{ $row->id_satuan }}">{{ $row->stn_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Kode Batch</label>
                            <input type="text" class="form-control m-input" name="sbr_kode_batch"
                                   value="{{ $edit->sbr_kode_batch }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Expired Date</label>
                            <input type="text" class="form-control m-input m_datepicker_1_modal" name="sbr_expired"
                                   value="{{ \app\Helpers\Main::format_date($edit->sbr_expired) }}" readonly>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Harga Jual</label>
                            <input type="text" class="form-control m-input input-numeral" name="sbr_harga_jual"
                                   value="{{ \app\Helpers\Main::format_number($edit->sbr_harga_jual) }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Konsinyasi</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="sbr_konsinyasi" value="yes" {{ $edit->sbr_konsinyasi_status == 'yes' ? 'checked':'' }}>
                                    Ya
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="sbr_konsinyasi" value="no" {{ $edit->sbr_konsinyasi_status == 'no' ? 'checked':'' }}>
                                    Tidak
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>